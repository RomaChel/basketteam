package lesson5;

public class PracticeWithRounding {

    public static void main(String[] args) {
        float num = 10.66f;

        System.out.println(Math.round(num));
        System.out.println(Math.floor(num));
        System.out.println(Math.ceil(num));
    }
}
