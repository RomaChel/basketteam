package lesson5;

public class TypeConversion {

    public static void main(String[] args) {
        test6();
    }

    private static void test1() {
        byte a = 15;
        byte b = a;
        System.out.println(b);
    }

    private static void test2() {
        byte a = 10;
        int b = a;
        System.out.println(b);
    }

    private static void test3() {
        int a = 0;
        long b = 20;
        a = (int) b;
        System.out.println(a);
    }

    private static void test4() {
        double a = 11.2354;
        int b = (int) a;
        System.out.println(b);
    }

    private static void test5() {
        double a = 130;
        byte b = (byte) a;
        System.out.println(b);
    }

    private static void test6() {
        char a = 'B';
        int b = (int) a;
        System.out.println(b);
    }
}
