package lesson5;

public class CycleTasks {

    public static void main(String[] args) {
        test5();

    }

    private static void test1() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(i + " ");
        }
    }

    private static void test2() {
        for (int i = 5; i >= 1; i--) {
            System.out.println(i + " ");
        }
    }

    private static void test3() {
        for (int i = 1; i <= 10; i++) {
            System.out.println("\t3*" + i + "=" + 3 * i);
        }
    }

    private static void test4() {
        int a = 7;
        while (a <= 98) {
            System.out.print(a + " ");
            a+=7; // а = а + 7
        }
    }

    private static void test5() {
        boolean change = false;
        boolean black = true;
        boolean white = false;
        if (black) {
            change = black;
        }
        if (white) {
            change = white;
        }
        if (change) {
            System.out.println("black");
        } else {
            System.out.println("white");
        }
    }

}
