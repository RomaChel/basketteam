package lesson5;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PracticeWithRandom {

    public static void main(String[] args) {
       // System.out.println(getRandomShop("CH", "DE", "AT", "BE", "GB"));
        System.out.println(randomMail("5260"));
        System.out.println(randomPass());

    }

    public static void test1() {
        double a = Math.random();
        System.out.println(a);
    }

    public static void test2() {
        double a = Math.random() * 3;
        System.out.println(a);
    }

    /*Допустим, нам необходимо получить вещественное число в интервале [ 20; 60) (60 исключительно)
     * Шаг №1: Диапазон [0;1) умножается на 40. Соответственно,
     * Нижняя граница: 0*40 = 0
     * Верхняя граница: 1*40 = 40
     * Получаем диапазон [0;40)
     * Шаг №2: К диапазону [0;40) прибавляем 20. Соответственно,
     * Нижняя граница: 0 + 20  = 20
     * Верхняя граница: 40 + 20 = 60
     * Получаем диапазон [20;60)
     * Если просто подставить в формулу, то получим:
     * ( Math.random() * (60 - 20) ) + 20  =>  Math.random() * 40 + 20
     * */
    public static void test3() {
        for (int i = 0; i < 50; i++) {
            int a = (int) (10 + Math.random() * 20);
            System.out.print(a + " ");
        }
    }

    /**
     * Пример как получить рпндомный шоп без использования метода random()
     * */
    public static String getRandomShop(String...shop) {
        List<String> randomShop = Arrays.asList(shop);
        Collections.shuffle(randomShop);
        return randomShop.get(0);
    }

    /**
     * Рассмотрим пример рандома с использованием класса Random
     * */
    public static String randomMail(String qcNumber) {
        Random randomGeneration = new Random();
        int random = randomGeneration.nextInt();
        return "qc_" + qcNumber + "_autotest" + random + "@mailinator.com";
    }

    public static String randomPass() {
        Random randomGeneration = new Random();
        int random = randomGeneration.nextInt();
        return String.valueOf("$" + "AAqq" + random);
    }

}
