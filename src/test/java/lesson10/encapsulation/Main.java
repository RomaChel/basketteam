package lesson10.encapsulation;

import java.util.Scanner;

public class Main {

    /**Нам нужна программа для приема в школу программирования. Ученики могут быть приняты в школу, если им исполнилось 6 лет.
     Нам дана программа, которая объявляет класс Pupil.

     Задача
     реализовать метод setAge класса Pupil. Если значение параметра age выше 6, назначьте его атрибуту age  и выведите "Welcome".
     В противном случае выведите "Sorry".

     Пример Входных Данных
     7

     Пример Выходных Данных
     Welcome*/

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter age: ");
        int age = scan.nextInt();

        Pupil pupil = new Pupil();
        pupil.setAge(age);
    }
}
