package lesson10.encapsulation;

public class Pupil {

    private int age;

    public void setAge(int age) {
        if (age >= 6) {
            System.out.println("Welcome");
        } else {
            System.out.println("Sorry");
        }
    }
}
