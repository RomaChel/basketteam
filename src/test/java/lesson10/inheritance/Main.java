package lesson10.inheritance;

public class Main {

    /**Вы разрабатываете собственное приложение для редактирования фото.
     Стандартная бесплатная версия позволяет рисовать и писать на фото.
     По подписке на Pro версию доступны 2 дополнительных функции:
     применение эффектов (useEffects) и изменение разрешения (changeResolution).

     Нам нужно, расширить подписку Pro так что бы у него появились все возможности включая подписку Standard и вызовать все методы.*/
    public static void main(String[] args) {
        Standard standard = new Standard();
        Pro pro = new Pro();

        //Стандартная версия
        standard.draw();
        standard.write();

        //Pro версия
        pro.draw();
        pro.write();
        pro.useEffects();
        pro.changeResolution();
    }
}
