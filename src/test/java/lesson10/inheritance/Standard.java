package lesson10.inheritance;

public class Standard {

    protected void draw() {
        System.out.println("Drawing");
    }

    protected void write() {
        System.out.println("Writing");
    }

}
