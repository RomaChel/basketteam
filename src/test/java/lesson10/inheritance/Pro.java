package lesson10.inheritance;

public class Pro extends Standard {

    protected void useEffects() {
        System.out.println("Using Effects");
    }

    protected void changeResolution() {
        System.out.println("Changing Resolution");
    }

    @Override
    protected void write() {
        super.write();
        System.out.println("QQQQQ");
    }
}
