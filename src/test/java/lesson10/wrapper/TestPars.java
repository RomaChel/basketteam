package lesson10.wrapper;


import static java.lang.Integer.parseInt;

public class TestPars {

    public static void main(String[] args) {
        String a = "150 b c D ".replaceAll("\\D", "");

        int b = parseInt(a);

        double c = Double.parseDouble(a);

        System.out.println(b + 150);
        System.out.println(c + 250);
    }
}
