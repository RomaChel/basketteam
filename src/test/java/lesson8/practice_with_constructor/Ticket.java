package lesson8.practice_with_constructor;

public class Ticket {

    private String movie;
    private int row , seat;

    public Ticket(String movie, int row, int seat) {
        this.movie = movie;
        this.row = row;
        this.seat = seat;
    }

    public Ticket(String movie) {
        this.movie = movie;
    }

    public String getMovie() {
        return movie;
    }

    public int getRow() {
        return row;
    }

    public int getSeat() {
        return seat;
    }
}
