package lesson8.practice_with_constructor;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter movie title: ");
        String movie = scan.nextLine();
        System.out.println("Enter the desired row: ");
        int row = scan.nextInt();
        System.out.println("Enter the desired seat: ");
        int seat = scan.nextInt();


        Ticket ticket = new Ticket(movie, row, seat);

        System.out.println("Movie: " + ticket.getMovie());
        System.out.println("Row: " + ticket.getRow());
        System.out.println("Seat: " + ticket.getSeat());
    }
}
