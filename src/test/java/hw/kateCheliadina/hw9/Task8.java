package hw.kateCheliadina.hw9;

import java.util.Scanner;

public class Task8 {

    /**Вы тур-менеджер, и вам нужна программа, которая определит маленькие страны.
     Страна считается маленькой, если ее население ниже 10000 и ее площадь меньше 10000 гектаров.
     Данная программа принимает население и площадь в качестве входных данных.

     Задача
     Завершите программу, чтобы выводить "small country", если удовлетворены оба условия. В противном случае не выводите ничего.

     Пример Входных Данных
     9955
     7522

     Пример Выходных Данных
     small country

     Подсказка:
     Используйте && для объединения условий.
     */

    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        int population = read.nextInt();
        int area = read.nextInt();
        //Завершите код

        if ((population < 10000) & (area < 10000)) {
            System.out.println("small country");
        } else {
            System.out.println("big country");
        }
    }
}
