package hw.kateCheliadina.hw9;

public class Task1 {

    /**
     * Вам дан код, который выводит характеристики автомобиля, но что-то не так.
     *
     * Задача
     * Исправьте код, чтобы выводить характеристики.
     *
     * Выходные данные
     * Name: Toyota
     * Engine: 4.7
     * Year: 2019
     */
    public static void main(String[] args) {
        //исправьте типы переменных
        String name = "Toyota";
        double engine = 4.7;
        int year = 2019;

        System.out.println("Name: " + name);
        System.out.println("Engine: " + engine);
        System.out.println("Year: " + year);

    }
}
