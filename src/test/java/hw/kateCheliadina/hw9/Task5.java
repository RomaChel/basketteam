package hw.kateCheliadina.hw9;

import java.util.Scanner;

public class Task5 {

    /**
     * Вам требуется программа для перевода дней в секунды.
     * Данный код использует количество дней в качестве вводных данных.
     * Завершите код для перевода количества дней в секунды и вывода результата.
     * <p>
     * Пример вводных данных:
     * 12
     * <p>
     * Пример результата:
     * 1036800
     * <p>
     * Пояснение: 12 дней – это 12*24 = 288 часов, то есть 288*60 = 17280 минут,
     * которые равны 17280*60 = 1036800 секундам.
     */
    public static void main(String[] args) {
        System.out.println("Введите количество дней");
        Scanner scanner = new Scanner(System.in);
        int days = scanner.nextInt();

        //введите код сюда
        int sec = days*24*60*60;
        System.out.println("Это секунд " + sec);

    }
}
