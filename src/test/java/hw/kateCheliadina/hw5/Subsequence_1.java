package hw.kateCheliadina.hw5;

public class Subsequence_1 {
    public static void main(String[] args) {
        sequence();
    }

    /*Task2. Необходимо вывести на консоль такую последовательность чисел:
    // 1 2 4 8 16 32 64 128 256 512*/
    public static void sequence(){
        for (int i = 1; i<=512; i=i*2){
              System.out.print(" " + i);
        }
    }
}
