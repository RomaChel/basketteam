package hw.kateCheliadina.hw5;

import java.util.Scanner;

public class Cikl_4 {
    public static final int VALUE_TO_STOP_CYCLE = 1; // константа хранит в себе значение
    public static void main(String[] args) {
//        numbermult1();
//        numbermult2();
        numbermult3();
    }

    //-----------------------------------СПОСОБ 1-----------------------------------
    public static void numbermult1(){
       int variable = 0;
       while (variable != 1) {
         System.out.println("Enter any integer number 1 ");
            Scanner scan = new Scanner(System.in); //объявили сканнер
            Integer number1 = scan.nextInt(); //запишется в number то, что юзер введет с консоли
            System.out.println("Enter any integer number 2 ");
            Integer number2 = scan.nextInt(); //запишется в number то, что юзер введет с консоли
            System.out.println("number1*number2= " + number1 * number2);
            System.out.println("\nDo you want exit? \n Yes - 1 \n No - any other number");
            variable = scan.nextInt();
       }
    }

    //-----------------------------------СПОСОБ 2-----------------------------------
    public static void numbermult2(){
        boolean exit=false;
        int ex = 0;
        while (!exit) {
            System.out.println("Enter any integer number 1 ");
            Scanner scan = new Scanner(System.in); //объявили сканнер
            Integer number1 = scan.nextInt(); //запишется в number то, что юзер введет с консоли
            System.out.println("Enter any integer number 2 ");
            Integer number2 = scan.nextInt(); //запишется в number то, что юзер введет с консоли
            System.out.println("number1*number2= " + number1 * number2);
            System.out.println("\nDo you want exit? \n Yes - 1 \n No - any other number");
            ex = scan.nextInt();
           if (ex == 1)
               exit = true;
        }
    }
    //-----------------------------------СПОСОБ 3-----------------------------------
    /**
     * method makes decision if program should proceed new cycle
     * @param value user's input
     * @return true if user's input doesn't equal to {@link Cikl_4#VALUE_TO_STOP_CYCLE}
     */
     static boolean proceedCycle(int value) {
        return value != VALUE_TO_STOP_CYCLE;
    }

    public static void numbermult3(){
        int variable = 0;
        while (proceedCycle(variable)) {

            System.out.println("Enter any integer number 1 ");
            Scanner scan = new Scanner(System.in); //объявили сканнер
            Integer number1 = scan.nextInt(); //запишется в number то, что юзер введет с консоли
            System.out.println("Enter any integer number 2 ");
            Integer number2 = scan.nextInt(); //запишется в number то, что юзер введет с консоли
            System.out.println("number1*number2= " + number1 * number2);
            System.out.println("\nDo you want exit? \n Yes - 1 \n No - any other number");
            variable = scan.nextInt();

        }
    }
}
