package hw.kateCheliadina.hw5;

import java.util.Scanner;

public class Sum_3 {
    public static void main(String[] args) {
        sum();
    }

    /*Task3. Напишите программу, где пользователь вводит любое целое положительное число.
    А программа суммирует все числа от 1 до введенного пользователем числа.
    Например:
    если пользователь введет число 3. Программа должна посчитать сумму чисел от 1 до 3, то есть 1+2+3 и выдать ответ 6.
    если пользователь введет число 5. Программа должна посчитать сумму чисел от 1 до 5, то есть 1+2+3+4+5 и выдать ответ 15.*/

    public static void sum() {
        System.out.println("Enter any integer positive number ");
        Scanner scan = new Scanner(System.in); //объявили сканнер
        Integer number = scan.nextInt(); //запишется в number то, что юзер введет с консоли

        if (number >= 0) {
            //цикл суммирования
            int summa = 0;
            for (int i = 1; i <= number; i++) {
                summa = summa + i;
            }
            System.out.println("  sum = " + summa);
        } else {
                 System.out.println("  Вы ввели число <0");
                 }
    }

}
