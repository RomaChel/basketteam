package hw.kateCheliadina.CalculatorCheliadina;

public class CalculatorMain {
    public static void main(String[] args) {
        Calculator calc =  new Calculator();
        double num1 = calc.getNumber();
        double num2 = calc.getNumber();
        char operation = calc.getOperation();
        double result = calc.calc(num1, num2, operation);
        System.out.println("Результат: " + result);
    }
}
