package hw.kateCheliadina.Group;

public class Teacher extends SchoolMember {
    @Override
    public String toString() {
        return "Teacher " + name + '\n' +
                "Groups: \n" + groups;
    }
}
