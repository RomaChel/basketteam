package hw.kateCheliadina.Group;

import java.util.List;

public class SchoolMember {
    public String name;
    private String lastName;
    private int age;
    private String gender;
    private String currentPosition;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StudyGroup> groups;

    public List<StudyGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<StudyGroup> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "SchoolMember{" +
                "name='" + name + '\'' +
                ", groups=" + groups +
                '}';
    }
}
