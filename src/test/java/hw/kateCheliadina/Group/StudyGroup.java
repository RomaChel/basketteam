package hw.kateCheliadina.Group;

public class StudyGroup {
    public String groupName;

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return groupName;
    }
}
