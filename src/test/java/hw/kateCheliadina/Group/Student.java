package hw.kateCheliadina.Group;

public class Student extends SchoolMember{
    @Override
    public String toString() {
        return "Student " + name + '\n' +
                "Groups: \n" + groups;
    }
}
