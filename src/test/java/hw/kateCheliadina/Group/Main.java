package hw.kateCheliadina.Group;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<StudyGroup> groupsStudent1 = new ArrayList();//групи студента Ivan //объявили экземпляр класса ArrayList под именем groupsStudent1
        List<StudyGroup> groupsStudent2 = new ArrayList();//групи студента Oleg
        List<StudyGroup> groupsTeacher1 = new ArrayList();//групи вчителя Stepan

        //Перший студент Іван
        Student student1 = new Student();
        student1.setName("Ivan");
        StudyGroup group1 = new StudyGroup();
        group1.setGroupName("Java Elementary (PROGRAMING)");//перша група для Івана //через метод add() добавили имя в groupsStudent1
        groupsStudent1.add(group1);

        StudyGroup group2 = new StudyGroup();
        group2.setGroupName("QA (TESTING)");//друга група для Івана
        groupsStudent1.add(group2);

        student1.setGroups(groupsStudent1);
        info(Collections.singletonList(student1));

        //Другий студент Олег
        Student student2 = new Student();
        student2.setName("Oleg");
        StudyGroup group3 = new StudyGroup();
        group3.setGroupName("Recruiting & HR(MANAGEMENT)");//одна група для Олега
        groupsStudent2.add(group3);

        student2.setGroups(groupsStudent2);
        info(Collections.singletonList(student2));

        //Вчитель Степан
        Teacher teacher1 = new Teacher();
        teacher1.setName("Stepan");
        groupsTeacher1.add(group1);//група для Стапана та сама група, що перша група у студента Івана

        teacher1.setGroups(groupsTeacher1);
        info(Collections.singletonList(teacher1));
    }

    public static void info(List<SchoolMember> schoolMembers){
        for (SchoolMember o : schoolMembers)
            System.out.println(o);
    }
}
