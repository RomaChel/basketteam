package hw.kateCheliadina.practice_with_get_set;

import java.util.Scanner;

public class Helper {
        public static String getName(Scanner scan){
        //Scanner scan = new Scanner(System.in);
        System.out.println("Enter the student name: ");
        String name = scan.nextLine();
        return name;
    }

    public static int getAge(Scanner scan){
        //Scanner scan = new Scanner(System.in);
        System.out.println("Enter the student age: ");
        int age = scan.nextInt();
        return age;
    }

    public static int getId(Scanner scan){
        //Scanner scan = new Scanner(System.in);
        System.out.println("Enter the student ID: ");
        int id = scan.nextInt();
        return id;
    }

}
