package hw.kateCheliadina.practice_with_get_set;

public class Student {

    private String name;
    private int age;
    private int id;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        if (age <= 0) {
            System.err.println("Invalid age!!!");
            this.age = 0;
        } else {
            this.age = age;
        }
    }

    public void setId(int id) {
        this.id = id;
    }
}
