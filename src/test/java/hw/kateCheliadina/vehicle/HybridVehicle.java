package hw.kateCheliadina.vehicle;

public class HybridVehicle extends Vehicle{

    @Override
    protected void resource() {
        //super.resource();
        System.out.println("I use both petrol and electricity");
    }
}
