package hw.kateCheliadina.vehicle;

    /*Транспортные средства можно разделить на классы, в зависимости от типа потребляемой энергии.
    Вам дана программа, определяющая 3 класса транспортных средств: Vehicle, ElectricVehicle и HybridVehicle.
    ElectricVehicle и HybridVehicle унаследованы от класса Vehicle.
    Завершите программу, повторно реализировав метод resource() в унаследованных классах, чтобы существующие вызовы работали верно.*/

public class Main {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle();
        HybridVehicle hybrid = new HybridVehicle();
        ElectricVehicle electric = new ElectricVehicle();

        //вызовы
        vehicle.start();
        vehicle.resource();
        electric.start();
        electric.resource();
        hybrid.start();
        hybrid.resource();
    }
}
