package hw.kateCheliadina;

import java.util.Scanner;

public class HW_lesson3 {

    public static void main(String[] args) {
        test1();
        test2();
    }

    /*1. Попросите пользователя ввести 2 любых целых числа. После этого необходимо вывести в консоль сумму этих 2 чисел*/
        public static void test1(){
            System.out.println("Task1. Попросите пользователя ввести 2 любых целых числа. После этого необходимо вывести в консоль сумму этих 2 чисел");
            System.out.println("Enter any integer number1: ");
            Scanner scan1 = new Scanner(System.in);
            int number1 = scan1.nextInt();
            System.out.println("Enter any integer number2: ");
            Scanner scan2 = new Scanner(System.in);
            int number2 = scan2.nextInt();
            int sum = number1 + number2;
            System.out.println("The sum of two entered numbers: " + number1 + "+" + number2 + "=" + sum);
         }

    /*2. Допустим мы хотим, чтобы пользователь вводил в консоль сразу несколько слов или фраз,
     а мы могли бы вывести ему одной строчкой, что же он ввёл.*/
        public static void test2(){
            System.out.println("------------------------------------------");
            System.out.println();
            System.out.println("Task2. Допустим мы хотим, чтобы пользователь вводил в консоль сразу несколько слов или фраз, а мы могли бы вывести ему одной строчкой, что же он ввёл.");
            System.out.println("Enter any two phrases using enter between them: ");
            Scanner scan1 = new Scanner(System.in); //объявляем сканер
            String line1 = scan1.nextLine(); //считываем текст 1
            String line2 = scan1.nextLine(); //считываем текст 2
            System.out.println("You were entered: " + line1 + " " + line2);
        }

}
