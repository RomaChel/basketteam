package hw.kateCheliadina;

public class ArrayPractice {
    public static void main(String[] args) {

        int[][] array = new int[2][2];

        array[0][0] = -4;
        array[0][1] = 18;

        array[1][0] = 22;
        array[1][1] = 3;


        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print("  [" + i + "]" + "[" + j + "] = " + array[i][j]);
            }
            System.out.println();
        }

        //поиск максимального элемента
        int max = array[0][0];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++){
                if (array[i][j] > max) {max = array[i][j];}
            }
        }
        System.out.println("max = " + max);

        //поиск минимального элемента
        int min = array[0][0];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++){
                if (array[i][j] < min) {min = array[i][j];}
            }
        }
        System.out.println("min = " + min);

        //сумма всех элементов
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++){
                sum = sum + array[i][j];
            }
        }
        System.out.println("sum = " + sum);

        //каждый элемент *2
        System.out.println("каждый элемент *2");
        int[][] array2 = new int[2][2];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++){
                array2[i][j] = array[i][j] * 2;
            }
        }
        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[i].length; j++) {
                System.out.print("  [" + i + "]" + "[" + j + "] = " + array2[i][j]);
            }
            System.out.println();
        }
    }
}
