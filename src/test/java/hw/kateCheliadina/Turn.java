package hw.kateCheliadina;

import java.util.Scanner;

public class Turn {
    public static void main(String[] args) {
        /*String str = "А роза упала на лапу Азора";
        way1toCharArray(str);
        way2charAt(str);
        way3StringBuffer(str);*/
        way3StringBuffer("А роза упала на лапу Азора");
    }


    public static void way1toCharArray(String str) {
        //String str = "Hello world";
        System.out.println("Way1. toCharArray \n String before:  " + str);
        // преобразовать строку в массив символов
        char[] newStr = str.toCharArray();
        String resStr = "";
        for (int i = newStr.length - 1; i >= 0; i--) {
            resStr = resStr + newStr[i];
        }
        System.out.println(" String after:  " + resStr);
    }

    public static void backwardsFor() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the word: ");
        String text = scanner.nextLine();
        char[] symbol = text.toCharArray();

        for (int x = symbol.length - 1; x >= 0; x--) {
            System.out.println(symbol[x]);
        }
    }

    public static void backwards() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the word: ");
        String text = scanner.nextLine();
        char[] arr = text.toCharArray();
        int a = arr.length;
        while (a > 0) {
            int i = a - 1;
            System.out.println(arr[i]);
            a--;
        }
    }

    public static String way2charAt(String str) {
        //String str = "Hello world";
        System.out.println("Way2. charAt \n String before:  " + str);
        int dlStr = str.length(); //длина введеной строки
        String resStr = "";
        for (int i = 0; i < dlStr; i++) {
            resStr = str.charAt(i) + resStr;
        }
        System.out.println(" String after:  " + resStr);
        return resStr;
    }

    public static void /*String*/ way3StringBuffer(String str) {
        System.out.println("Way3. StringBuffer \n String before:  " + str);
        String resStr = new StringBuffer(str).reverse().toString();  //это я подсмотрела в интернете
        System.out.print(" String after:  " + resStr);
        /*return resStr;*/
    }
}
