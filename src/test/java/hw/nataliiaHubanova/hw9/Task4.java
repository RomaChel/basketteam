package hw.nataliiaHubanova.hw9;

import java.util.Scanner;

public class Task4 {

    /**
     * Том и Боб играют в настольную игру, в начале которой обоим игрокам дается равное количество очков.
     * Том победил в первом раунде и заработал 1 очко, а Боб проиграл.
     * Данная программа должна взять начальное количество очков и выполнить первый раунд,
     * увеличив на 1 количество очков у Тома и уменьшив на 1 у Боба.
     * Но что-то пошло не так: программа выводит результаты без изменений.
     * <p>
     * Задание
     * Исправьте ошибку, чтобы программа выдавала правильный результат.
     * <p>
     * Пример исходных данных:
     * 5
     * <p>
     * Пример результата:
     * Round 1 results:
     * 6
     * 4
     * <p>
     * Пояснение:
     * В начале игры о обоих игроков было по 5 очков.
     * После первого раунда у Тома стало на 1 очко больше (6, первый результат),
     * а Боб потерял 1 очко (4, второй результат).
     */

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("принятие начальных очков");
        int initScore = scanner.nextInt();
        int scoreTom = initScore;
        int scoreBob = initScore;

        System.out.println("Round 1 results:");
        //исправьте код
        System.out.println(scoreTom);
        System.out.println(scoreBob);
    }
}
