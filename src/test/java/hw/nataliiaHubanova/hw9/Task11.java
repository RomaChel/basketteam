package hw.nataliiaHubanova.hw9;

import java.util.Scanner;

public class Task11 {

    /**Вам дан код, который принимает в качестве входных данных количество студентов, которые поступили в университет.
     Давайте их поздравим!

     Задача
     Завершите программу, чтобы вывести "Welcome" для каждого студента.

     Пример Входных Данных
     2

     Пример Выходных Данных
     Welcome
     Welcome*/

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество студентов:");
        int n = scanner.nextInt();
        // ваш код
    }
}
