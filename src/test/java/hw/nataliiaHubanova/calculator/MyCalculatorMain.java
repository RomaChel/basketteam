package hw.nataliiaHubanova.calculator;


public class MyCalculatorMain {

    public static void main(String[] args) {
        MyCalculator calculator = new MyCalculator();
        double num1 = calculator.getNumber();
        double num2 = calculator.getNumber();
        char operation = calculator.getOperation();
        double result  = calculator.calc(num1, num2, operation);
        System.out.println("Результат: " + result);

    }
}

