package hw.nataliiaHubanova.calculator;

import java.util.Scanner;

public class MyCalculator {
    public static double getNumber () {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число:");
        if (scan.hasNextDouble()) {
            return scan.nextDouble();
        } else {
            System.out.println("Ошибка при вводе. Повторите ввод");
            return getNumber();
        }
    }

    public static char getOperation () {
        Scanner scan = new Scanner(System.in);
        System.out.println("Выберите номер операции:\n1 + \n2 - \n3 * \n4 / ");
        int operationNumber;
        if (scan.hasNextInt()) {
            operationNumber = scan.nextInt();
        } else {
            System.out.println("Вы ввели не существующую операцию! Повторите ввод!");
            return getOperation();
        }
        switch (operationNumber) {
            case 1:
                return '+';
            case 2:
                return '_';
            case 3:
                return '*';
            case 4:
                return '/';
            default:
                System.out.println("Неправильный оператор! Повторите ввод!");
                return getOperation();
        }
    }

    private static double add (double num1, double num2) {
        return num1+num2;
    }

    private static double sub(double num1, double num2) {
        return num1-num2;
    }
    private static double mult(double num1, double num2) {
        return num1*num2;
    }
    private static double div(double num1, double num2) {
        if(num2 == 0) {
            System.out.println("Делить на 0 нельзя!!! Введите другое число");
            return getOperation();
        }else
            return num1/num2;
    }
    public static double calc(double num1, double num2, char operation) {
        switch (operation) {
            case '+':
                return add(num1, num2);
            case '-':
                return sub(num1, num2);
            case '*':
                return mult(num1, num2);
            case '/':
                return div(num1, num2);
            default:
                System.out.println("Ошибка операции. Повторите ввод");
                return calc(num1, num2, operation);
        }
    }
}
