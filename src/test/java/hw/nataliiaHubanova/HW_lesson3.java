package hw.nataliiaHubanova;

import java.util.Scanner;

public class HW_lesson3 {
    public static void main(String[] args) {
        test1();
        test2();
    }
  public static void test1() {
      System.out.println("Введите два любых целых числа: ");
      Scanner scan = new Scanner(System.in);
      int number1 = scan.nextInt();
      int number2 = scan.nextInt();
      int sum = number1 + number2;
      System.out.println("Сумма этих чисел равна " + sum);
  }

  public static void test2() {
      System.out.println("Введите несколько слов или фразу");
      Scanner scan = new Scanner(System.in);
      String line1 = scan.nextLine();
      String line2 = scan.nextLine();
      String line3 = scan.nextLine();
      System.out.println("Вы ввели фразу " + line1 + " " + line2 + " " + line3);


  }
}



