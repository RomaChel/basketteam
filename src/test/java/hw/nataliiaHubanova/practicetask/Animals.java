package hw.nataliiaHubanova.practicetask;

public abstract class Animals {

    protected String name;
    protected int age;

    public Animals(String name,int age) {
        this. name = name;
        this.age = age;
    }

    public abstract void sound();

    public abstract void myAge();

}
