package hw.nataliiaHubanova.practicetask;

public class Dog extends Animals {

    public Dog (String name,int age) {
        super(name, age);
    }

    @Override
    public void sound() {
        System.out.println("I'am a Doc. My name is " + name + ". Gav");
    }

    @Override
    public void myAge() {
        System.out.println("I am " + age + " years old.");

    }
}

