package hw.nataliiaHubanova.hw_lesson7;

public class MultiplicationSchoolTable {
    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j < 10; j++) {
                System.out.print((i * j) + "\t");// вывод ряда чисел, разделенных знаком табуляции
            }
            System.out.println();
        }
        System.out.println("----------------------------------------------");
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j < 10; j++) {
                System.out.printf("%d\t", i * j);
            }
            System.out.println();
        }
    }
}
