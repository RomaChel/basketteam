package hw.nataliiaHubanova.hw_lesson7;

import java.util.Scanner;

public class ProgramOfNumbersMultiplying {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n;
        do {
            System.out.println("Введите целое число: ");
            int num1 = scan.nextInt();
            System.out.println("Введите второе целое число: ");
            int num2 = scan.nextInt();
            System.out.println(num1 * num2);

            System.out.println("Завершить программу? Для выхода из программы нажмите 1. Для продолжения работы - нажмите любую цифру");
            n = scan.nextInt();
        }
        while (n != 1);
        scan.close();
    }

}
