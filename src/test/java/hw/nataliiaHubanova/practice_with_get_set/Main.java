package hw.nataliiaHubanova.practice_with_get_set;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the student name: ");
        String name = scan.nextLine();
        System.out.println("Enter the student age: ");
        int age = scan.nextInt();
        System.out.println("Enter the student id: ");
        int id = scan.nextInt();

        Student student1 = new Student();
        student1.setName(name);
        student1.setAge(age);
        student1.setId(id);

        System.out.println("Name: " + student1.getName());
        System.out.println("Age: " + student1.getAge());
        System.out.println("ID: " + student1.getId());

       /* Student student2 = new Student();
        student2.setName(name);
        student2.setAge(age);
        student2.setId(id);

        System.out.println(student2.getName());
        System.out.println(student2.getAge());
        System.out.println(student2.getId());

        Student student3 = new Student();
        student3.setName(name);
        student3.setAge(age);
        student3.setId(id);

        System.out.println(student3.getName());
        System.out.println(student3.getAge());
        System.out.println(student3.getId());*/

    }
}
