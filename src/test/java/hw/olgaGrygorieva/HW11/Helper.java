package hw.olgaGrygorieva.HW11;

import java.util.Scanner;

public class Helper {
    public static String getNameStudent() {
        System.out.println(" Введите имя");
        Scanner scanner = new Scanner(System.in);
        String StudentName = scanner.nextLine();
        return StudentName;
    }

    public static int getAgeStudent() {
        System.out.println(" Введите возраст");
        Scanner scanner = new Scanner(System.in);
        int StudentAge = scanner.nextInt();
        return StudentAge;
    }

    public static int getIdStudent() {
        System.out.println(" Введите ID");
        Scanner scanner = new Scanner(System.in);
        int StudentId = scanner.nextInt();
        return StudentId;
    }
}
