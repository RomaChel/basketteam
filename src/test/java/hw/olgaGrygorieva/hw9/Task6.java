package hw.olgaGrygorieva.hw9;

import java.util.Scanner;

public class Task6 {

    /**
     * У вас есть $12 000 на покупку машины.
     * Вам дана программа, которая принимает цену машины в качестве входных данных.
     * <p>
     * Задача
     * Выведите "yes", если цена ниже или равна 12 000.
     * <p>
     * Пример Входных Данных
     * 11000
     * <p>
     * Пример Выходных Данных
     * yes
     * <p>
     * Используйте оператор if для проверки требуемого условия
     */

    public static void main(String[] args) {
        System.out.println("введите вашу сумму: ");
        Scanner scanner = new Scanner(System.in);
        int price = scanner.nextInt();
        //завершите код
        if (price <= 12000) {
            System.out.println(" YES ");
        } else {
            System.out.println("Сумма не равна и не меньше 12000");
        }
    }
}
