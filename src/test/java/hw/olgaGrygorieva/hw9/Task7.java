package hw.olgaGrygorieva.hw9;

import java.util.Scanner;

public class Task7 {

    /**
     * Менеджер по продажам решил предоставлять подарочную карту покупателям, совершившим покупки на сумму более 15000.
     * Кроме того, клиенты, общая сумма покупок которых превышает 30 000, получат вторую подарочную карту.
     * Вам дана программа, которая принимает сумму покупки в качестве входных данных и выводит "Gift card",
     * если она  выше 15000.
     * <p>
     * Задача
     * Завершите код, чтобы выводить "Gift card" еще раз, если сумма покупок выше 30000.
     * <p>
     * Пример Входных Данных
     * 36000
     * <p>
     * Пример Выходных Данных
     * Gift card
     * Gift card
     * <p>
     * Подсказка:
     * Добавьте оператор if внутри существующего для обработки второго условия.
     */

    public static void main(String[] args) {
        System.out.println("введите сумму покупки ");
        Scanner read = new Scanner(System.in);
        int purchases = read.nextInt();

        if (purchases > 15000) {
            System.out.println("Gift card");
            //Завершите код
            if (purchases > 30000) {
                System.out.println("Gift card");
            }
        } else {
            System.out.println(" make an order more than 15000 and receive the Gift card");
        }
    }
}
