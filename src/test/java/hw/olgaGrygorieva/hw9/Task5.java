package hw.olgaGrygorieva.hw9;

import java.util.Scanner;

public class Task5 {

    /**
     * Вам требуется программа для перевода дней в секунды.
     * Данный код использует количество дней в качестве вводных данных.
     * Завершите код для перевода количества дней в секунды и вывода результата.
     * <p>
     * Пример вводных данных:
     * 12
     * <p>
     * Пример результата:
     * 1036800
     * <p>
     * Пояснение: 12 дней – это 12*24 = 288 часов, то есть 288*60 = 17280 минут,
     * которые равны 17280*60 = 1036800 секундам.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //введите код сюда
        System.out.println("Введите количество дней:");
        int days = scanner.nextInt();
        int hours = days * 24;
        int minutes = hours * 60;
        int seconds = minutes * 60;
        System.out.println("Количество дней в секундах: " + seconds );

    }
}
