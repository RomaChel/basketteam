package hw.olgaGrygorieva.hw9;

public class Task3 {

    /**
     * Бармен продал 64 бутылки пива и 23 бутылки виски. Вам нужно рассчитать, сколько всего бутылок было продано.
     * Задача:
     * Рассчитайте и выведите общее количество проданных бутылок.
     * */
    public static void main(String[] args) {
        int beer = 64;
        int whisky = 23;
        int sum = beer + whisky;
        System.out.println( "Всего продано бутылок: " + sum);//рассчитайте сумму и выведите ее
    }
}
