package hw.olgaGrygorieva.hw9;

import java.util.Scanner;

public class Task8 {

    /**
     * Вы тур-менеджер, и вам нужна программа, которая определит маленькие страны.
     * Страна считается маленькой, если ее население ниже 10000 и ее площадь меньше 10000 гектаров.
     * Данная программа принимает население и площадь в качестве входных данных.
     * <p>
     * Задача
     * Завершите программу, чтобы выводить "small country", если удовлетворены оба условия. В противном случае не выводите ничего.
     * <p>
     * Пример Входных Данных
     * 9955
     * 7522
     * <p>
     * Пример Выходных Данных
     * small country
     * <p>
     * Подсказка:
     * Используйте && для объединения условий.
     */

    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        System.out.println("введите население страны");
        int population = read.nextInt();
        System.out.println("введите площадь страны");
        int area = read.nextInt();
        //Завершите код
        if (population < 10000 && area < 10000) {
            System.out.println(" small country ");
        } else {
            System.out.println(" ");
        }
    }
}
