package hw.olgaGrygorieva.HW12;

public class Vehicle {

    protected void resource() {
        System.out.println("I use petrol");
    }

    protected void start() {
        System.out.println("You can see different types of vehicles");
    }
}
