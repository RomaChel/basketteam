package hw.olgaGrygorieva.HW12;

//Транспортные средства можно разделить на классы, в зависимости от типа потребляемой энергии. Вам дана программа, определяющая 3 класса транспортных средств: Vehicle, ElectricVehicle и HybridVehicle.
//ElectricVehicle и HybridVehicle унаследованы от класса Vehicle.
//Завершите программу, повторно реализировав метод resource() в унаследованных классах, чтобы существующие вызовы работали верно.

public class Main {
    public static void main(String[] args) {

        Vehicle vehicle = new Vehicle();
        Vehicle electric = new ElectricVehicle();
        Vehicle hybrid = new HybridVehicle();


        //вызовы
        //vehicle
        vehicle.start();
        vehicle.resource();

        //electric
        electric.start();
        electric.resource();

        //hybrid
        hybrid.start();
        hybrid.resource();
    }
}