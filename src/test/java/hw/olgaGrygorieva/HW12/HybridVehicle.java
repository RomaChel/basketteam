package hw.olgaGrygorieva.HW12;

public class HybridVehicle extends Vehicle {
    @Override
    protected void resource() {
        System.out.println("I use both petrol and electricity");
    }

    protected void start(){
        super.start();

    }

}
