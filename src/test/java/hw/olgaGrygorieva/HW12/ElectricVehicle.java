package hw.olgaGrygorieva.HW12;

public class ElectricVehicle extends Vehicle {
    @Override
    protected void resource() {
        System.out.println("I use electricity");
    }

    protected void start() {
        super.start();
    }
}