package hw.olgaGrygorieva.HW_lesson5;

import java.util.Scanner;

//3.Напишите программу, где пользователь вводит любое целое положительное число.
//А программа суммирует все числа от 1 до введенного пользователем числа.
// Например:
//если пользователь введет число 3. Программа должна посчитать сумму чисел от 1 до 3, то есть 1+2+3 и выдать ответ 6.
//если пользователь введет число 5. Программа должна посчитать сумму чисел от 1 до 5, то есть 1+2+3+4+5 и выдать ответ 15.

public class SumOfNumbers {

    public static void main(String[] args) {
        System.out.print("ВВедите любое целое число: ");
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        int sum = 0;
        for (int a = 1; a <= num; a++) {
            sum = sum + a;
        }
        System.out.println(sum);
    }

}
