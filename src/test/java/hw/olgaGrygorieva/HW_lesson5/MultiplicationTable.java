package hw.olgaGrygorieva.HW_lesson5;

//2.Напишите программу, которая выводит на консоль таблицу умножения

public class MultiplicationTable {
    public static void main(String[] args) {
        multiplication_table();
    }

    public static void multiplication_table() {
        for (int i = 1; i < 10; i++) {
            for (int k = 1; k < 10; k++) {
                System.out.print("\t" + (i * k));
            }
            System.out.println();
        }
    }
}
