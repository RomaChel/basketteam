package hw.olgaGrygorieva.HW_lesson5;
//1.Необходимо вывести на консоль такую последовательность чисел:  1 2 4 8 16 32 64 128 256 512

public class NumbersLine {
    public static void main(String[] args) {
        task1();
        System.out.println();

    }
    public static void task1() {
        int v = 1;
        while (v <= 512) {
            System.out.print(v + " ");
            v = v * 2;

        }
    }
}

