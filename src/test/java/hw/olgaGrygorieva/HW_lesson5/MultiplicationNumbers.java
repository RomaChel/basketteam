package hw.olgaGrygorieva.HW_lesson5;

import java.util.Scanner;

//4.
//В цикле программа просит у пользователя ввести два числа и выводит результат их умножения.  После вывода результата умножения программа спрашивает, надо ли завершить выполнение.
//Если пользователь введет число 1, то программа завершает цикл.
//Если введено любое другое число, то программа продолжает спрашивать у пользователя два числа и умножать их.


public class MultiplicationNumbers {

    public static void main(String[] args) {
        multiplication();
    }

    public static void multiplication() {
        System.out.println("введите первое любое целое число  ");
        Scanner scan = new Scanner(System.in);

        int number1 = scan.nextInt();
        System.out.println(" вы ввели число " + number1);

        System.out.println("введите второе любое целое число ");
        int number2 = scan.nextInt();
        System.out.println(" вы ввели число " + number2);

        int multiplication = number1 * number2;
        System.out.println(" результат умножения ваших чисел: " + multiplication);

        System.out.println("хотите завершить выполнение программы?  \n1 - да \nлюбое целое число - нет");
        int num = scan.nextInt();

        if (num == 1) {
            System.out.println(" программа завершена ");
        } else {
            multiplication();
        }
    }

}