package hw.olgaGrygorieva.practicetask;

public class Cat extends Animals {


    public Cat(String name, int age) {
        super(name, age);
    }

    @Override
    public void sound() {

        System.out.println("I'am a Cat. My name is " + name + ". Mew");

    }

    @Override
    public void myAge() {

        System.out.println("I am " + age + " years old.");

    }
}
