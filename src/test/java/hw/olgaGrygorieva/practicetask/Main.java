package hw.olgaGrygorieva.practicetask;


import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String... args) {
       List<Animals> animals = new ArrayList<Animals>();
        animals.add(new Dog("Tuzik", 15));
        animals.add(new Cat("Vasya", 10));

        for (Animals animal : animals) {
            animal.sound();
            animal.myAge();
            // I'am a Doc. My name is Tuzik. Gav
            // I am 15 years old.
            // I'am a Cat. My name  Vasya. Mew
            // I am 10 years old.
        }
    }
}
