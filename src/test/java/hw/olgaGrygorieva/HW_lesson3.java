package hw.olgaGrygorieva;

import java.util.Scanner;

public class HW_lesson3 {

    public static void main(String[] args) {
        sum();
        story();
    }
    //1. Попросите пользователя ввести 2 любых целых числа. После этого необходимо вывести в консоль сумму этих 2 чисел//

    public static void sum() {
        System.out.println("введите первое любое целое число  ");
        Scanner scan = new Scanner(System.in);
        int number1 = scan.nextInt();
        System.out.println(" вы ввели число " + number1);

        System.out.println("введите второе любое целое число ");
        int number2 = scan.nextInt();
        System.out.println(" вы ввели число " + number2);

        int sum = number1 + number2;
        System.out.println(" сумма ваших чисел " + sum);

    }

    //2. Допустим мы хотим, чтобы пользователь вводил в консоль сразу несколько слов или фраз, а мы могли бы вывести ему одной строчкой, что же он ввёл.//
    public static void story() {
        System.out.println(" Введите любое слово или фразу ");
        Scanner scan = new Scanner(System.in);
        String line = scan.nextLine();
        System.out.println(line);

        System.out.println(" Введите еще любое слово или фразу ");
        String line1 = scan.nextLine();
        System.out.println(line1);

        String sentence = line + " " + line1;
        System.out.println(" у вас получилось предложение: " + sentence + ".");
    }

}
