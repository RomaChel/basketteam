package hw.olgaGrygorieva.calculator;

import java.util.Scanner;

public class Calculator {
    public static double getNumber() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число:");
        if (scan.hasNextDouble()) {
            return scan.nextDouble();
        } else {
            System.out.println("ошибка при вводе. повторите ввод");
            return getNumber();
        }
    }

    public static char getOperation() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Выберете номер операции: \n1 -прибавить\n2 - отнять\n3 -умножить\n4 -разделить");
        int operationNumber;
        if (scan.hasNext()) {
            operationNumber = scan.nextInt();
        } else {
            System.out.println("Вы ввели несуществующую операция! повторите ввод");
            return getOperation();
        }
        switch (operationNumber) {
            case 1:
                return '+';
            case 2:
                return '-';
            case 3:
                return '*';
            case 4:
                return '/';
            default:
                System.out.println("неправильный оператор! повторите ввод!");
                return getOperation();
        }
    }

    private static double add(double num1, double num2) {
        return num1 + num2;
    }

    private static double sub(double num1, double num2) {
        return num1 - num2;
    }

    private static double multiplication(double num1, double num2) {
        return num1 * num2;
    }

    private static double division(double num1, double num2) {
        if (num2 != 0) {
            return num1 / num2;
        } else {
            System.out.println(" на 0 делить нельзя ");
        return Double.NaN;

        }
    }

    public static double calc(double num1, double num2, char operation) {
        switch (operation) {
            case '+':
                return add(num1, num2);
            case '-':
                return sub(num1, num2);
            case '*':
                return multiplication(num1, num2);
            case '/':
                return division(num1, num2);

            default:
                System.out.println("ошибка операции. Повторите ввод");
                return calc(num1, num2, operation);
        }

    }

}
