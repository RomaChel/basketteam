package hw.olgaGrygorieva.calculator;

public class CalculatorMain {

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        double num1 = calculator.getNumber();
        double num2 = calculator.getNumber();
        char operation = calculator.getOperation();
        double result = calculator.calc(num1, num2, operation);
        System.out.println("результат: " + result);
    }
}
