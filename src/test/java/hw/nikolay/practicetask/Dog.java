package hw.nikolay.practicetask;

public class Dog extends Animals{
    public Dog(String name, int age) {
        super(name, age);
    }

    @Override
    protected void sound() {
        System.out.println( "I'am a Doc. My name is " + name +  " Gav");
    }

    @Override
    protected void myAge() {
        System.out.println( "I am Doc " + age +  " years old." );
    }
}
