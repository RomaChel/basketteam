package hw.nikolay.practicetask;

public abstract class Animals {

    protected String name;
    protected int age;

    public Animals(String name, int age) {
        this.name = name;
        this.age = age;
    }



    protected abstract void sound();

    protected abstract void myAge();
}
