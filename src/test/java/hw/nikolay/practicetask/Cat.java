package hw.nikolay.practicetask;

public class Cat extends Animals{
    public Cat(String name, int age) {
        super(name, age);
    }

    @Override
    protected void sound() {
        System.out.println( "I'am a Cat. My name is " + name +  " May");
    }

    @Override
    protected void myAge() {
        System.out.println( "I am Cat " + age +  " years old." );
    }


}
