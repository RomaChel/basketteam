package hw.nikolay.hw_5;

public class LoopOutputTwo {

    public static void main(String[] args) {
        loopOutputTwo();
    }

    public static void loopOutputTwo() {
        int a = 1;
        while (a <= 512) {
            System.out.print(a + " ");
            a *= 2;

        }
    }
}
