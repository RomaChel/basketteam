package hw.nikolay.hw_5;

import java.util.Scanner;

public class MultiplicationOfNumbers {

    public static void main(String[] args) {
        multiplicationOfNumbers();
    }

    public static void multiplicationOfNumbers() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите два целочисленных числа: ");
        int numberOne = scanner.nextInt();
        int numberTwo = scanner.nextInt();

        System.out.println("Сумма умноженных чисел равна: " + numberOne * numberTwo + "\n " +
                "Если хотите продолжить игру нажмите - 1 " + "\n " +
                "Если не хотите продолжать играть нажмите - 2 ");

        int nextGame = scanner.nextInt();

        if (nextGame == 1){
            multiplicationOfNumbers();
        }if (nextGame == 2){
            System.out.println("Игра закончена");
        }else if (nextGame != 1 || nextGame != 2){
            System.out.println("Вы ввели не верное значение");
            multiplicationOfNumbers();
        }
    }
}
