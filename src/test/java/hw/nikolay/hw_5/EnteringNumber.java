package hw.nikolay.hw_5;

import java.util.Scanner;

public class EnteringNumber {

    public static void main(String[] args) {
            enteringNumberUser();
    }

    public static void enteringNumberUser() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите любое целочисленное число : ");
        int getNumberUser = scanner.nextInt();
        int sum = 0;
        for (int i = 0; i <= getNumberUser; i++) {
            sum = sum + i;
        }
        System.out.println("Вашь ответ: " + sum);
    }

}
