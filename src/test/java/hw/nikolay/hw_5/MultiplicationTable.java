package hw.nikolay.hw_5;

public class MultiplicationTable {

    public static void main(String[] args) {
        multiplicationTable();
    }

    public static void multiplicationTable() {
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                int sum = i * j;
                System.out.print("\t" + sum);
            }
            System.out.println();
        }
    }
}
