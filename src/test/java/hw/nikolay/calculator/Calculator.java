package hw.nikolay.calculator;

import java.util.Scanner;

public class Calculator {

    public static double getNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число: ");
        if (scanner.hasNextDouble()) {
            return scanner.nextDouble();
        } else {
            System.out.println("Ошибка при вводе повторите ввод");
            return getNumber();
        }

    }

    public static char getOperation() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Выберите номер операции:\n1 - прибавить\n2 - отнять\n3 - умножить\n4 - поделить");
        int operationNumber;
        if (scanner.hasNextInt()) {
            operationNumber = scanner.nextInt();
        } else {
            System.out.println("Вы ввели не существующую операцию, повторите ввод");
            return getOperation();
        }
        switch (operationNumber) {
            case 1:
                return '+';
            case 2:
                return '-';
            case 3:
                return '*';
            case 4:
                return '/';
            default:
                System.out.println("Не првельный оператор повторите ввод");
                return getOperation();

        }

    }

    private static double add(double num1, double num2) {   // добавить два числа
        return num1 + num2;

    }

    private static double minus(double num1, double num2) {  // отнять два числа
        return num1 - num2;
    }

    private static double multiplication(double num1, double num2) {  // умножить два числа
        return num1 * num2;
    }

    private static double division(double num1, double num2, char operation) {    // поделить два числа

        if (num2==0) {                                                           // проверка что на 0 делить нельзя
            System.out.println("Делить на 0 нельзя!");
            return getOperation();
        }else
            return num1/num2;
    }


    public static double calc(double num1, double num2, char operation) {
        switch (operation) {
            case '+':
                return add(num1, num2);
            case '-':
                return minus(num1, num2);
            case '*':
                return multiplication(num1, num2);
            case '/':
                return division(num1, num2, operation);
            default:
                System.out.println("Ошибка операции. Повторите ввод");
                return calc(num1, num2, operation);
        }
    }
}
