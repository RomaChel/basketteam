package hw.nikolay.practice_with_get_set;

public class Main extends Helper {

    public static void main(String[] args) {
    studentMax();
    studentVasya();
    studentJulia();
    studentGrisha();
    studentIgor();
    }

    public static void studentMax() {
        Student studentMax = new Student();
        studentMax.setName(getNameUsers());
        studentMax.setAge(getAgeUsers());
        studentMax.setId(getIdUsers());

        System.out.println("Ваше имя: " + studentMax.getName());
        System.out.println("Ваш возврост " + studentMax.getAge());
        System.out.println("Ваше Id: " + studentMax.getId());
    }

    public static void studentVasya() {
        Student studentVasya = new Student();
        studentVasya.setName(getNameUsers());
        studentVasya.setAge(getAgeUsers());
        studentVasya.setId(getIdUsers());

        System.out.println("Ваше имя: " + studentVasya.getName());
        System.out.println("Ваш возврост " + studentVasya.getAge());
        System.out.println("Ваше Id: " + studentVasya.getId());
    }

    public static void studentJulia() {
        Student studentJulia = new Student();
        studentJulia.setName(getNameUsers());
        studentJulia.setAge(getAgeUsers());
        studentJulia.setId(getIdUsers());

        System.out.println("Ваше имя: " + studentJulia.getName());
        System.out.println("Ваш возврост " + studentJulia.getAge());
        System.out.println("Ваше Id: " + studentJulia.getId());
    }

    public static void studentGrisha() {
        Student studentGrisha = new Student();
        studentGrisha.setName(getNameUsers());
        studentGrisha.setAge(getAgeUsers());
        studentGrisha.setId(getIdUsers());

        System.out.println("Ваше имя: " + studentGrisha.getName());
        System.out.println("Ваш возврост " + studentGrisha.getAge());
        System.out.println("Ваше Id: " + studentGrisha.getId());
    }

    public static void studentIgor() {
        Student studentIgor = new Student();
        studentIgor.setName(getNameUsers());
        studentIgor.setAge(getAgeUsers());
        studentIgor.setId(getIdUsers());

        System.out.println("Ваше имя: " + studentIgor.getName());
        System.out.println("Ваш возврост " + studentIgor.getAge());
        System.out.println("Ваше Id: " + studentIgor.getId());
    }
}
