package hw.nikolay.practice_with_get_set;

import java.util.Scanner;

public class Helper {

    public static String getNameUsers(){
        System.out.println("введите ваше имя");
        Scanner scanner = new Scanner(System.in);
        String userName = scanner.nextLine();
        return userName;
    }

    public static int getAgeUsers(){
        System.out.println("введите вашь возрост");
        Scanner scanner = new Scanner(System.in);
        int userAge = scanner.nextInt();
        return userAge;
    }

    public static int getIdUsers(){
        System.out.println("введите ваше ID");
        Scanner scanner = new Scanner(System.in);
        int userId = scanner.nextInt();
        return userId;
    }
}
