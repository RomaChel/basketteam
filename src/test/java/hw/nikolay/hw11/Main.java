package hw.nikolay.hw11;

public class Main {
    public static void main(String[] args) {

        Vehicle vehicle = new Vehicle();
        Vehicle electric = new ElectricVehicle();
        Vehicle hybrid = new HybridVehicle();

        vehicle.start();
        vehicle.resource();

        electric.start();
        electric.resource();

        hybrid.start();
        hybrid.resource();
    }
}
