package hw.nikolay;

import java.util.Scanner;

public class HW_lesson3 {
    public static void main(String[] args) {
        getNumberSum();
        getTextUser();
    }

    /*
        1. Попросите пользователя ввести 2 любых целых числа.
        После этого необходимо вывести в консоль сумму этих 2 чисел
*/

    public static void getNumberSum() {
        System.out.println("Введите любые два целочисленные числа");

        Scanner scanner = new Scanner(System.in);


        System.out.println("Введите первое число: ");
        int getUserFirstNumber = scanner.nextInt();                // получение первого числа от юзера

        System.out.println("Введите второе число: ");
        int getUserSecondNumber = scanner.nextInt();               // получение второго числа от юзера

        int sumOfNumbers = getUserFirstNumber + getUserSecondNumber;    // сумма двух чисел юзера

        System.out.println("Сумма двух числе равна: " + sumOfNumbers);
    }



      /*
        2. Допустим мы хотим, чтобы пользователь вводил в консоль сразу несколько слов или фраз,
         а мы могли бы вывести ему одной строчкой, что же он ввёл.
*/

    public static void getTextUser() {
        System.out.println("Ввведите любую фразу:");

        Scanner scanner = new Scanner(System.in);

        String getUserText = scanner.nextLine();           // получение фразы юзера

        System.out.println("Вы ввели фразу: " + getUserText);
    }

}
