package hw.annaGlov.hw_lesson10;

public class ElectricVehicle extends Vehicle {

    @Override
    protected void resource() {
        System.out.println("I use electricity");
    }
}
