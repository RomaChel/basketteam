package hw.annaGlov.hw_lesson10;

public class HybridVehicle extends Vehicle {

    @Override
    protected void resource() {
        System.out.println("I use both petrol and electricity");
    }
}
