package hw.annaGlov.hw_lesson10;

public class Vehicle {

    protected void start() {
        System.out.println("I'am a machine with wheels and an engine for transporting people or goods, especially on land");
    }

    protected void resource() {
        System.out.println("I use petrol");
    }
}
