package hw.annaGlov;

import java.util.Scanner;

public class HW_lesson7 {
    public static void main(String[] args) {
        String text;
        System.out.println("Введите слово или фразу");
        Scanner scan = new Scanner(System.in);
        text = scan.nextLine();
        System.out.println("Введенная вами фраза в обратном порядке: ");
        arrayReverse1(text);
        arrayReverse2(text);
        arrayReverse3(text);
    }

    public static void arrayReverse1(String text) {
        char[] result = text.toCharArray();
        for (int i = result.length - 1; i >= 0; i--) {
            System.out.print(result[i]);
        }
    }

    public static void arrayReverse2(String text) {
        char[] result = text.toCharArray();
        int a = result.length;
        while (a > 0) {
            int i = a - 1;
            System.out.print(result[i]);
            a--;
        }
    }
    public static void arrayReverse3(String text) {
        String result = new StringBuffer(text).reverse().toString();
        System.out.println(result);
    }
}