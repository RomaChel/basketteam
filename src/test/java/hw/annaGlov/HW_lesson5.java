package hw.annaGlov;

import java.util.Scanner;

public class HW_lesson5 {
    public static void main(String[] args) {
        exercise1();
    }

    private static void exercise1() {
        int a = 1;
        while (a <= 512) {
            System.out.print(a + " ");
            a *= 2;
        }
    }

    private static void exercise2() {
        for (int i = 1; i <= 9; i++) {
            System.out.print(i);
            for (int j = 2; j <= 9; j++) {
                System.out.print("\t" + i * j);
            }
            System.out.println();
        }
    }

    public static void exercise3() {
        System.out.println("Введите любое целое положительное число: ");
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        int sum = 0;
        for (int b = 1; b <= number; b++) {
            sum = sum + b;
        }
        System.out.println(sum);
    }

    public static void exercise4() {
        int result;
        do {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введите первое целое число");
            int n1 = scan.nextInt();
            System.out.println("Введите второе целое число");
            int n2 = scan.nextInt();
            System.out.println("Результатом умножения введенных чисел является: " + (n1 * n2));
            System.out.println("Введите 1, если необходимо завершить выполнение или любое число для продолжения");
            result = scan.nextInt();
        }
        while (result != 1);
    }
}
