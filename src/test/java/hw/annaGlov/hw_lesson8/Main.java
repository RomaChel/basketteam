package hw.annaGlov.hw_lesson8;

public class Main {
    public static void main(String[] args) {
        int n = Helper.getStudentsNumber();
        Student [] students = new Student[n];
        for (int i = 0; i < n; i++) {
            Student student = new Student();
            student.setName(Helper.getName());
            student.setAge(Helper.getAge());
            student.setId(Helper.getId());
            students[i] = student;
        }
        for (Student student : students){
            System.out.println("Name: " + student.getName());
            System.out.println("Age: " + student.getAge());
            System.out.println("ID: "+ student.getId());
            System.out.println("-------------------------");
        }
    }
}
