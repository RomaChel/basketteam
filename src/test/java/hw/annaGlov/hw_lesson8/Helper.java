package hw.annaGlov.hw_lesson8;

import java.util.Scanner;

public class Helper {
    public static int getStudentsNumber() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter number of students: ");
        return scan.nextInt();
    }

    public static String getName() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter student name: ");
        return scan.nextLine();
    }

    public static int getAge() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter student age: ");
        return scan.nextInt();
    }

    public static int getId() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter student ID: ");
        return scan.nextInt();
    }
}
