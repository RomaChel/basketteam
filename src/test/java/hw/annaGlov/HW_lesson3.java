package hw.annaGlov;

import java.util.Scanner;

public class HW_lesson3 {
    public static void main(String[] args) {
        exercise1();
        exercise2();
    }

    public static void exercise1() {
        System.out.println("Введите первое целое число");
        Scanner scan = new Scanner(System.in);
        int number1 = scan.nextInt();
        System.out.println("Введите второе целое число");
        int number2 = scan.nextInt();
        System.out.println("Сумма введенных чисел равна: " + (number1 + number2));
    }

    public static void exercise2() {
        System.out.println("Введите слово или часть фразы");
        Scanner scan = new Scanner(System.in);
        String phrase1 = scan.nextLine();
        System.out.println("Введите следующее слово или часть фразы");
        String phrase2 = scan.nextLine();
        System.out.println("Введите следующее слово или часть фразы");
        String phrase3 = scan.nextLine();
        System.out.println("Вы ввели фразу: " + phrase1 + " " + phrase2 + " " + phrase3);
    }
}
