package hw.annaGlov.studyGroup;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Student studentIvan = new Student("Ivan");
        Student studentOleg = new Student("Oleg");
        Teacher teacherStepan = new Teacher("Stepan");

        Group javaElementary = new Group(GroupName.JAVA_ELEMENTARY);
        javaElementary.addStudentToGroup(studentIvan);
        javaElementary.addTeacherToGroup(teacherStepan);

        Group qa = new Group(GroupName.QA);
        qa.addStudentToGroup(studentIvan);

        Group hr = new Group(GroupName.HR);
        hr.addStudentToGroup(studentOleg);

        groupsOutput(Arrays.asList(studentIvan, studentOleg, teacherStepan));
    }
    public static void groupsOutput(List<Person> people) {
        for (Person person: people) {
            System.out.println(person);
        }
    }
}
