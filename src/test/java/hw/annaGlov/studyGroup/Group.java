package hw.annaGlov.studyGroup;

import java.util.ArrayList;
import java.util.List;

public class Group {
    private final GroupName groupName;
    private Teacher teacher;
    private List<Student> students = new ArrayList<>();

    public Group(GroupName groupName) {
        this.groupName = groupName;
    }

    public void addTeacherToGroup(Teacher teacher) {
        this.teacher = teacher;
        teacher.addStudyGroup(this);
    }

    public void addStudentToGroup(Student student){
        students.add(student);
        student.addStudyGroup(this);
    }

    @Override
    public String toString() {
        return groupName.getGroupName();
    }
}
