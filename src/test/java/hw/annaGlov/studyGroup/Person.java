package hw.annaGlov.studyGroup;

import java.util.ArrayList;
import java.util.List;

public abstract class Person {
    private String name;
    private String personType;
    private List<Group> groups = new ArrayList<>();
    public Person(String name, String personType) {
        this.name = name;
        this.personType = personType;
    }
    public void addStudyGroup (Group group) {
        groups.add(group);
    }

    @Override
    public String toString() {
        return personType + " " + name + "\n" + "Groups:\n" + groups;
    }
}
