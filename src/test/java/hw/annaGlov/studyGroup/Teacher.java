package hw.annaGlov.studyGroup;

public class Teacher extends Person {
    public Teacher(String name) {
        super(name, "Teacher");
    }
}
