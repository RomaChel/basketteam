package hw.annaGlov.studyGroup;

public enum GroupName {

    JAVA_ELEMENTARY("Java Elementary (PROGRAMMING)"),
    QA("QA (TESTING)"),
    HR("Recruiting & HR (MANAGEMENT)");

    private final String groupName;

    GroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }
}