package hw.annaGlov.calculator;

import java.util.Scanner;

public class AGCalculator {

        public static double getNumber() {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введите число: ");
            if (scan.hasNextDouble()) {
                return scan.nextDouble();
            } else {
                System.out.println("Ошибка при вводею Повторите ввод");
                return getNumber();
            }
        }

        public static char getOperation() {
            Scanner scan = new Scanner(System.in);
            System.out.println("Выберите номер операции: \n1 - прибавить\n2 - отнять\n3 - умножить\n4 - разделить");
            int operationNumber = 0;
            if (scan.hasNextInt()) {
                operationNumber = scan.nextInt();
            } else {
                System.out.println("Вы ввели несуществующую операцию. Повторите ввод!");
                return getOperation();
            }
            switch (operationNumber) {
                case 1:
                    return '+';
                case 2:
                    return '-';
                case 3:
                    return '*';
                case 4:
                    return '/';
                default:
                    System.out.println("Неправильный оператор! Повторите ввод!");
                    return getOperation();
            }
        }

        private static double add(double num1, double num2) {
            return num1 + num2;
        }

        private static double sub(double num1, double num2) {
            return num1 - num2;
        }

        private static double multi(double num1, double num2) {
            return num1 * num2;
        }

        private static double divide(double num1, double num2) {
            if (num2 == 0) {
                System.out.println("На 0 делить нельзя!Введите другое число");
                return -0;
            } else {
                return num1 / num2;
            }
        }


        public static double calc(double num1, double num2, char operation) {
            switch (operation) {
                case '+':
                    return add(num1, num2);
                case '-':
                    return sub(num1, num2);
                case '*':
                    return multi(num1, num2);
                case '/':
                    return divide(num1, num2);
                default:
                    System.out.println("Ошибка операции. Повторите ввод!");
                    return calc(num1, num2, operation);
            }
        }

    }

