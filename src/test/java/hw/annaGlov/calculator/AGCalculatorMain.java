package hw.annaGlov.calculator;

public class AGCalculatorMain {
    public static void main(String[] args) {
        AGCalculator calculator = new AGCalculator();
        double num1 = calculator.getNumber();
        double num2 = calculator.getNumber();
        char operation = calculator.getOperation();
        double result = calculator.calc(num1, num2, operation);
        System.out.println("Результат: " + result);

    }
}

