package hw.annaGlov.practiceTask;

public abstract class Animals {

    private String name;
    private int  age;

    public Animals(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void sound() {
        System.out.print("My name is " + name + ".");
    }

    public void myAge() {
        System.out.println("I am " + age + " years old.");
    }
}