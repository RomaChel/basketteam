package hw.annaGlov.practiceTask;

public class Cat extends Animals {

    public Cat(String name, int age) {
        super(name, age);
    }
    @Override
    public void sound() {
        System.out.print("I am a Cat. ");
        super.sound();
        System.out.println(" Mew");
    }
}