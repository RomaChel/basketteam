package hw.annaGlov.practiceTask;

public class Dog extends Animals {

    public Dog(String name, int age) {
        super(name, age);
    }
    @Override
    public void sound() {
        System.out.print("I am a Dog. ");
        super.sound();
        System.out.println(" Gav");
    }
}
