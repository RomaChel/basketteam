package lesson11.enum_example;

public class Dog {


    private final int PAWS = 4;
    private final int TAIL = 1;
    private String name;
    private String breed;
    private Size size = Size.UNDEFINED;
    private static int dogsCount;

    public Dog() {
        dogsCount++;
        System.out.println("Количество собак: " + dogsCount);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public void bark() {
        switch(size) {
            case BIG:
            case VERY_BIG:
                System.out.println("Wof - Wof");
                break;
            case AVERAGE:
                System.out.println("Raf - Raf");
                break;
            case SMALL:
            case VERY_SMALL:
                System.out.println("Tiaf - Tiaf");
                break;
            default:
                System.out.println("Размер собаки не задан!!!");
        }
    }

    public void bite() {
        if (dogsCount > 2) {
            System.out.println("Вас кусают собаки!!!");
        } else {
            bark();
        }
    }
}
