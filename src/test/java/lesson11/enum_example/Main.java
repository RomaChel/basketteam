package lesson11.enum_example;

public class Main {

    public static void main(String[] args) {
        Dog lab = new Dog();
        lab.setName("Chary");
        lab.setBreed("Lab");
        lab.setSize(Size.BIG);
        lab.bite();

        Dog alabai = new Dog();
        alabai.setName("Aly");
        alabai.setBreed("mops");
        alabai.setSize(Size.VERY_BIG);
        alabai.bite();

        Dog mops = new Dog();
        mops.setName("Mopi");
        mops.setBreed("mops");
        mops.setSize(Size.SMALL);
        mops.bite();
    }
}
