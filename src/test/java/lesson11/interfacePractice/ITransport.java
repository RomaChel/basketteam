package lesson11.interfacePractice;

public interface ITransport {

    void start();

    void stop();

    default void sound() {
        System.out.println("BiBiBI");
    }
}
