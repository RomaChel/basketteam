package lesson11.interfacePractice;

public enum EngineType {

    PETROL, DIESEL, ELECTRIC
}
