package lesson11.interfacePractice;

public class Bus extends Car implements ITransport, IDoors{


    public Bus(String producer, String model, EngineType engineType) {
        super(producer, model, engineType);
    }

    @Override
    public void myDoors() {
        System.out.println("Автобус едет.");
    }

    @Override
    public void start() {
        System.out.println("Автобус остановился.");
    }

    @Override
    public void stop() {
        System.out.println("У автобуса есть раздвижные двери.");
    }


}
