package lesson11.interfacePractice;

public class Truck extends Car implements ITransport, IDoors{


    public Truck(String producer, String model, EngineType engineType) {
        super(producer, model, engineType);
    }

    @Override
    public void myDoors() {
        System.out.println("Грузовик едет.");
    }

    @Override
    public void start() {
        System.out.println("Грузовик остановился.");
    }

    @Override
    public void stop() {
        System.out.println("У грузовика 2 двери.");
    }
}
