package lesson11.interfacePractice;

public class Sedan extends Car implements ITransport, IDoors{


    public Sedan(String producer, String model, EngineType engineType) {
        super(producer, model, engineType);
    }

    @Override
    public void start() {
        System.out.println("Автомобиль едет.");
    }

    @Override
    public void stop() {
        System.out.println("Автомобиль остановился.");
    }

    @Override
    public void myDoors() {
        System.out.println("У седана 4 двери.");
    }
}
