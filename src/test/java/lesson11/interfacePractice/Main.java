package lesson11.interfacePractice;

public class Main {

    public static void main(String[] args) {
        Sedan mercedes = new Sedan("Mercedes", "C-200", EngineType.PETROL);
        mercedes.start();
        mercedes.stop();;
        mercedes.myDoors();
        mercedes.sound();
        System.out.println(mercedes);

        Bus bagdan = new Bus("Bagdan", "QWS-250", EngineType.DIESEL);
        bagdan.start();
        bagdan.stop();
        bagdan.myDoors();
        System.out.println(bagdan);
    }
}
