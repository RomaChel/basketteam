package lesson11.interfacePractice;

public abstract class Car {

    private String producer;
    private String model;
    private EngineType engineType;

    public Car(String producer, String model, EngineType engineType) {
        this.producer = producer;
        this.model = model;
        this.engineType = engineType;
    }

    @Override
    public String toString() {
        return "Car{" +
                "producer='" + producer + '\'' +
                ", model='" + model + '\'' +
                ", engineType=" + engineType +
                '}';
    }
}
