package lesson11.abstract_example;

public abstract class Animals {

    public abstract void eat();

    public abstract void voice();

    public void walkAround(String place) {
        System.out.println("Я гуляю по " + place);
    }
}
