package lesson11.abstract_example;

public class Main {

    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.eat();
        cat.voice();
        cat.walkAround("Квартире");

        Dog dog = new Dog();
        dog.eat();
        dog.voice();
        dog.walkAround("Парку");

        Pig pig = new Pig();
        pig.eat();
        pig.voice();
        pig.walkAround("Лесу");
    }
}
