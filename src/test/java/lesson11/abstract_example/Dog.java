package lesson11.abstract_example;

public class Dog extends Animals {

    @Override
    public void eat() {
        System.out.println("Я ем мясо");
    }

    @Override
    public void voice() {
        System.out.println("Гав");
    }
}
