package lesson11.abstract_example;

public class Cat extends Animals {

    @Override
    public void eat() {
        System.out.println("Я ем рыбу");
    }

    @Override
    public void voice() {
        System.out.println("Мяу");
    }
}
