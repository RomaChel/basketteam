package lesson11.abstract_example;

public class Pig extends Animals {

    @Override
    public void eat() {
        System.out.println("Я ем жолуди");
    }

    @Override
    public void voice() {
        System.out.println("Хрю - Хрю");
    }
}
