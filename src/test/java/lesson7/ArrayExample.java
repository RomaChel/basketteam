package lesson7;

import java.util.Scanner;

public class ArrayExample {

    public static void main(String[] args) {
        arrayScan();


        /*int[] array1 = {1, 3, 5, 30, 80, 100};
        byte[] array2 = {1, 4};
        String[] citiesArray = {"Одесса", "КИев", "Львов", "Харьков"};

        int k = array1.length;
        System.out.println("Длина массива array1: " + k);

        int[] array3 = new int[10]; // int[] array3 = {0,0,0}
        array3[0] = 5;
        array3[1] = 17;
        array3[2] = 100;
        array3[9] = 200;

        System.out.println(array3[0]);
        System.out.println(array3[1]);
        System.out.println(array3[2]);
        for (int i = 0; i < array3.length; i++) {
            System.out.println(array3[i]);
        }*/
    }

    public static void testArrFalse() {
        boolean[] array = new boolean[1000];

        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }

    public static void testArrTrue() {
        boolean[] arr = new boolean[1000];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = true;
        }
        for (boolean b : arr) {
            System.out.println(b);
        }
    }

    public static void testArrTrueFalse() {
        boolean[] arr = new boolean[1000];
        for (int i = 0; i < arr.length; i = i + 2) {
            arr[i] = true;
        }
        for (boolean b : arr) {
            System.out.println(b);
        }
    }

    public static void arraySum() {
        int[] array = new int[3];
        array[0] = 5;
        array[1] = 17;
        array[2] = 350;

        int sum = 0;
        for (int i : array) {
            sum += i;
        }

        System.out.println("Сумма все значений: " + sum);
    }

    public static void arrayScan() {
        int number;
        int sum = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Total number of elements: ");
        number = scanner.nextInt();
        int[] array = new int[number];
        System.out.println("Enter the elements of the array: ");
        for (int i = 0; i < number; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println("Sum array elements: ");
        for (int i = 0; i < number; i++) {
            sum += array[i];
        }
        System.out.println(sum);
    }
}
