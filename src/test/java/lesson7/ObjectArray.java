package lesson7;

public class ObjectArray {

   static Cat[] cat = new Cat[3];

    public static void main(String[] args) {
        cat[0] = new Cat("Kitty");
        cat[1] = new Cat("Murka");
        cat[2] = new Cat("Vasy");

        for (int i = 0; i < cat.length; i++) {
            System.out.println(cat[i].name);
        }
    }
}
