package lesson7;

public class TwoDimensionalArray {

    public static void main(String[] args) {
        matrix();

        /*int[][] array = new int[2][2];

        array[0][0] = 1;
        array[0][1] = 2;

        array[1][0] = 10000;
        array[1][1] = 50000;


        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(" [" + i + "]" + "[" + j + "] = " + array[i][j]);

            }
            System.out.println();
        }*/
    }

    public static void whichOf() {
        String array[][] = {{"Hey", "John", "Bye"}, {"Josh", "Jonson", "Hello"}};

        System.out.println(array[1][1]);
    }

    public static void editTheValue() {
        int[][] myArr = {{1,2,3}, {10}, {5,6,7}};
        //int a = myArr[1][0];
        System.out.println(myArr[1][0]);
        myArr[1][0] = 42;
       // int x = myArr[1][0];
        System.out.println(myArr[1][0]);
    }

    public static void matrix() {
        int[][] matrix = {
                {8,1,6},
                {3,5,9},
                {4,7,0},
                {10,7,5},
                {4,7,0},
                {4,7,0},
                {4,7,0},
                {4,7,0}
        };
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
