package lesson2;

public class PersonMain {


    public static void main(String[] args) {
        Person person1 = new Person(); // Создаём экземпляр класса Person
        // задаем значения для объекта
        person1.setName("Natali");
        person1.setAge(25);
        person1.setId(12345);
        person1.setPhone("+380965239548");
        // выводим значения объекта
        System.out.println("Name: " + person1.getName());
        System.out.println("Age: " + person1.getAge());
        System.out.println("ID: " + person1.getId());
        System.out.println("Phone: " + person1.getPhone());

        System.out.println("---------------------------------------------------------");

        Person person2 = new Person(); // Создаём экземпляр класса Person
        // задаем значения для объекта
        person2.setName("Nikita");
        person2.setAge(30);
        person2.setId(123874);
        person2.setPhone("+380936542458");
        // выводим значения объекта
        System.out.println("Name: " + person2.getName());
        System.out.println("Age: " + person2.getAge());
        System.out.println("ID: " + person2.getId());
        System.out.println("Phone: " + person2.getPhone());
    }
}
