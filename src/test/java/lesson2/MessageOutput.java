package lesson2;

public class MessageOutput {

    public static void main(String[] args) {
        output1("JAVA");
        output2();
        output3();
        output4();
    }

    public static void output1(String message) {
        System.out.println("Я изучаю");
        System.out.println(message);
        System.out.println();
    }

    public static void output2() {
        System.out.println("Я изучаю");
        System.out.println("JAVA");
        System.out.println("Ура!!!");
        System.out.println();
    }

    public static void output3() {
        System.out.print("Я изучаю" + " " + "Наташ" + " ");
        System.out.print("JAVA");
        System.out.println();
    }

    public static void output4() {
        System.out.print("Я изучаю ");
        System.out.print("JAVA");
        System.out.println();
    }
}
