package lesson9;

public class Main2 {

    public static void main(String[] args) {
        CarForExample.myStatic += 20;

        CarForExample car = new CarForExample();
        System.out.println("Cтатическая переменная = " + car.myStatic);
        System.out.println("Не Cтатическая переменная = " + car.notStatic);

        System.out.println("Cтатическая переменная = " + car.myStatic);
        car.notStatic += 20;
        System.out.println("Не Cтатическая переменная = " + car.notStatic);

        CarForExample car2 = new CarForExample();
        CarForExample.myStatic += 100;
        car2.notStatic += 500;
        System.out.println("Cтатическая переменная = " + car2.myStatic);
        System.out.println("Не Cтатическая переменная = " + car2.notStatic);

        CarForExample car3 = new CarForExample();
        System.out.println("Cтатическая переменная = " + car3.myStatic);
        System.out.println("Не Cтатическая переменная = " + car3.notStatic);
    }
}
