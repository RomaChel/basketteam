package lesson13;

import java.io.FileWriter;
import java.io.IOException;

public class PracticeWithTryCatch {

    public static void main(String[] args) {
      /*  try {
            FileWriter fileWriter = new FileWriter("out.txt");
            fileWriter.write("Hello World");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Мы попали в блок catch");
        } finally {
            System.out.println("Я выполняюсь всегда!!!");
        }
        System.out.println("Программа работает успешно!!!");*/

        int a = 5;
        int b = 0;
        try {
            int c = a / b;
        } catch (ArithmeticException e) {
            System.out.println("Делить на ноль нельзя!");
        }
        System.out.println("Программа работает успешно!");
    }
}
