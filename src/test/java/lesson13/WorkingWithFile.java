package lesson13;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class WorkingWithFile {


    public static void main(String[] args) throws IOException {
       /* writeNumbersToFile(1, 5);
        writeStringToFile("Хокку \nПодобен лучу самурайский клинок \nИ тот затупился \nПроклятая килька в томате!!");

        readerFileAndWriterToNewFile( "string.txt");*/


        writeStringToFile(String.valueOf(createFile("D:", "basketFile", "firstFile.txt")),
                "Хокку \nПодобен лучу самурайский клинок \nИ тот затупился \nПроклятая килька в томате!!");
    }


    public static void writeNumbersToFile(int k1, int k2) throws IOException {
        FileWriter fr = new FileWriter("numbers.txt");

        for (int i = k1; i <= k2; i++) {
            fr.write(i + "\n");
        }
        fr.close();
    }

    public static void writeStringToFile(String text) throws IOException {
        FileWriter fr = new FileWriter("string.txt");
        fr.write(text);
        fr.close();
    }
    public static void writeStringToFile(String pathToFile, String text) throws IOException {
        FileWriter fr = new FileWriter(pathToFile);
        fr.write(text);
        fr.close();
    }


    public static void readerFileAndWriterToNewFile(String nameFileForReader) throws IOException {
        FileReader fr = new FileReader(nameFileForReader);
        Scanner sc = new Scanner(fr);
        int i = 0;

        while (sc.hasNextLine()) {
            i++;
            System.out.println(i + " : " + sc.nextLine());

        }
        fr.close();
    }

    public static File createFile(String disc, String folderName, String fileNameWithFormat) throws IOException {
        String fileSeparator = System.getProperty("file.separator");
        String absoluteFilePath = disc + fileSeparator + folderName + fileSeparator + fileNameWithFormat;
        File file = new File(absoluteFilePath);
        if (file.createNewFile()) {
            System.out.println(absoluteFilePath + " File created");
        } else {
            System.out.println("File " + absoluteFilePath + " already exists") ;
        }
        return file;
    }
}
