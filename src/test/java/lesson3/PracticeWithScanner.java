package lesson3;


import java.util.Scanner;

public class PracticeWithScanner {

    public static void main(String[] args) {
        test1();
        test2();
        test3();
        test4();
    }

    public static void test1() {
        System.out.println("Введите любое целое число от 1 до 10: ");
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        System.out.println("Вы ввели число " + number);
    }

    public static void test2() {
        System.out.println("Введите любое слово или фразу: ");
        Scanner scan = new Scanner(System.in);
        String line = scan.nextLine();
        System.out.println("Вы ввели: " + line);
    }

    public static void test3() {
        System.out.println("Введите любое слово или фразу: ");
        Scanner scan = new Scanner(System.in);
        String line = scan.next();
        System.out.println("Вы ввели: " + line);
    }

    public static void test4() {
        System.out.print("Введите любое дробное число: ");
        Scanner scan = new Scanner(System.in);
        double number = scan.nextDouble();
        System.out.println("Вы ввели число " + number);
    }
}
