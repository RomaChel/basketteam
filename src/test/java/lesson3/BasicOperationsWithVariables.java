package lesson3;

public class BasicOperationsWithVariables {

    public static void main(String[] args) {
        int k = 70;
        int p = 10;
        int f = 5;

        int m = k + p;
        System.out.println(m);

        m = m - 30;
        System.out.println(m);

        int s = m * f;
        System.out.println(s);

        float g = s / 3f;
        System.out.println(g);

        //-------------------------------------------------------------
        // обычная форма
        int a = 2;
        a = a + 7;
        System.out.println(a);
        //сокращенная форма
        int t = 2;
        t += 7;
        System.out.println(t);

        //-------------------------------------------------------------
        // деление по модулю
        int n = 9;
        int q = 4;
        int w = n % q;
        System.out.println(w);

        float u = 7.6f;
        float i = 2.9f;
        float o = u % i;
        System.out.println(o);

        int x = -9;
        int d = -4;
        int h = x % d;
        System.out.println(h);

        //----------------------------------------------------------------
        // инкримент и декримент
        int j = 2;
        j++;
        System.out.println(j);

        int l = 2;
        l--;
        System.out.println(l);

        //----------------------------------------------------------------

        int n1 = 2;
        int k1 = 2;
        int a1 = 2 * n1--;

        int b1 = 2 * --k1;

        System.out.println(a1);
        System.out.println(n1);
        System.out.println(b1);
        System.out.println(k1);
    }
}
