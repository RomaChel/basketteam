package lesson3;

public class StringConcatenation {

    public static void main(String[] args) {
        test1();
        test2();
        test3();
    }

    public static void test1() {
        String morning1 = "доброе";
        String morning2 = "не доброе, потому что сеголня суббота";

        System.out.println("Сегодня утро " + morning1);
        System.out.println("Сегодня утро " + morning2);
    }

    public static void test2() {
        String name = "Виктория";
        String car = "Marcedes-Bens C-200";
        System.out.println(name + " купила " + car);
    }

    public static void test3() {
        String sentence = "У меня есть цель на ближайший год.";
        sentence = sentence.concat(" Я должен(-на) изучить Java.");
        System.out.println(sentence + " Тогда я буду крут!!!");
        sentence = sentence.concat(" Тогда я буду крут!!!" + 1 + "год!");
        System.out.println(sentence);

    }



}
