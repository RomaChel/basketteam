package lesson3;

public class PracticeConstants {

    public static void main(String[] args) {
        calculateTheCirclePerimeter(50, 150);
    }

    public static void calculateTheCirclePerimeter(int radius1, int radius2) {
        final double PI = 3.1415926536;
        double length1 = 2 * PI * radius1;
        double length2 = 2 * PI * radius2;
        System.out.println("Периметр круга при радиусе " + radius1 + " см равен: " + length1);
        System.out.println("Периметр круга при радиусе " + radius2 + " см равен: " + length2);
    }
}
