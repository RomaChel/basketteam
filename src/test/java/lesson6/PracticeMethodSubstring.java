package lesson6;

public class PracticeMethodSubstring {

    public static void main(String[] args) {
        String sentence = "Доброе утро!";
        System.out.println(sentence.substring(6));
        System.out.println(sentence.substring(0, 7));
    }
}
