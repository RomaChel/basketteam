package lesson6;

public class PracticeMethodToUpperLoverCase {

    public static void main(String[] args) {
        String str = "I love Java";
        String result = str.toUpperCase();
        System.out.println("Result: " + result);

        str = "1 and 2 and 3";
        result = str.toUpperCase();
        System.out.println("Result: " + result);

        str = "AUTOMATION";
        result = str.toUpperCase();
        System.out.println("Result: " + result);

        str = "I love Java";
        result = str.toLowerCase();
        System.out.println("Result: " + result);

        str = "1 and 2 and 3";
        result = str.toLowerCase();
        System.out.println("Result: " + result);

        str = "AUTOMATION";
        result = str.toLowerCase();
        System.out.println("Result: " + result);
    }
}
