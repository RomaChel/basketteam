package lesson6.stringExample;

public class Human {

    private String name;

    public Human(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Человек с именем " + name;
    }
}
