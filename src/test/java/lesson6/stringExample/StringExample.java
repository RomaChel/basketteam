package lesson6.stringExample;

public class StringExample {

    public static void main(String[] args) {
        Human max = new Human("Max");
        String out = "Java объект: " + max;
        System.out.println(out);

        Human natali = new Human("Natali");
        String out2 = "Java объект: " + natali;
        System.out.println(out2);
    }
}
