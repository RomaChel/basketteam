package lesson6;

public class PracticeMethodToCharArray {

    public static void main(String[] args) {
        String str = "ABC";
        char[] result = str.toCharArray(); //{'A', 'B', 'C'};

        for (int i = 0; i < result.length; i++) {
            System.out.println("Element [" + i + "]:" + result[i]);
        }
    }
}
