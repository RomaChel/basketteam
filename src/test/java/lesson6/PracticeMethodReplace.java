package lesson6;

public class PracticeMethodReplace {

    public static void main(String[] args) {
        String oldString = "ABC";
        String newString = oldString.replace('A', 'G');

        System.out.println("Old string: " + oldString);
        System.out.println("New string: " + newString);
    }
}
