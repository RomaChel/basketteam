package lesson6;

public class PracticeMethodEquals {

    public static void main(String[] args) {
        String line1 = "Good morning!";
        String line2 = "Good morning!";
        String line3 = "Good evening!";

        System.out.println("String " + line1 + " equals " + line2 + " : " + line1.equals(line2));
        System.out.println("String " + line1 + " equals " + line3 + " : " + line1.equals(line3));

        String str = "10";
        Integer num = 10;

        System.out.println("String " + str + " equals " + num + " : " + str.equals(String.valueOf(num)));

        String str2 = "Test String";
        System.out.println("test string".equalsIgnoreCase(str2));
    }
}
