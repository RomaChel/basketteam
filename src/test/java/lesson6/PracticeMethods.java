package lesson6;

public class PracticeMethods {

    public static void main(String[] args) {
        showResult("Show sun: ", 5, 10);
        showResult("Show sun: ", 5, 55);
        showResult("Show sun: ", 5, 15);
        showResult("Show sun: ", 5, 101);
        showResult("Show sun: ", 5, 95);
        showResult("Show sun: ", 5, 1516);
        showResult("Show sun: ", 5, 85);
        showResult("Show sun: ", 5, 55);
        showResult("Show sun: ", 5, 126);
    }


    public static int getSum(int x, int y) {
        return x + y;
    }

    public static String getText(String text) {
        return text;
    }

    public static void showResult(String text, int x, int y) {
        System.out.println(getText(text) + getSum(x, y));
    }
}
