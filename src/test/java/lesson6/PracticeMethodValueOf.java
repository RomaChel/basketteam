package lesson6;

public class PracticeMethodValueOf {

    public static void main(String[] args) {
        String a = String.valueOf(1);
        String b = String.valueOf(12.0);
        String c = String.valueOf(1235.45F);
        String d = String.valueOf(12325545L);
        String s = String.valueOf(true);
        String v = String.valueOf(false);
        String human = String.valueOf(new Human("Alex"));

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(s);
        System.out.println(v);
        System.out.println(human);
        System.out.println(new Human("Alex"));
    }

    static class Human {
        private String name;

        public Human(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
