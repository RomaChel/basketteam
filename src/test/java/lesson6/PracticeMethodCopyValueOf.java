package lesson6;

public class PracticeMethodCopyValueOf {

    public static void main(String[] args) {
        char[] chars = {'I', ' ', 'w', 'a', 'n', 't', ' ', 't', 'o', ' ', 'b', 'e', ' ', 'a', ' ',
                'd', 'e', 'v', 'e', 'l', 'o', 'p', 'e', 'r', '.'};

        String s1 = "String";
        System.out.println("String s1 before copyValueOf: " + s1);

        s1 = s1.copyValueOf(chars);

        System.out.println("String s1 after copyValueOf: " + s1);
        char[] chars2 = {'J', 'a', 'v', 'a'};

        String s2 = String.copyValueOf( chars2 );
        System.out.println("Returned String: " + s2);
    }
}
