package lesson6;

public class PracticeMethodContains {

    public static void main(String[] args) {
        String str1 = "Java string contains If else Example";
        if (str1.contains("Example")) {
            System.out.println("The Keyword :example: is found in given string");
        } else {
            System.out.println("The Keyword :example: is not found in given string");
        }
    }
}
