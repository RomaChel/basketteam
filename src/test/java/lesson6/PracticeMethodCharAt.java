package lesson6;

public class PracticeMethodCharAt {

    public static void main(String[] args) {
        String sentence = "Я скоро буду профи в Java";
        char ch1 = sentence.charAt(0);
        char ch2 = sentence.charAt(21);
        char ch3 = sentence.charAt(7);
        System.out.println("Символ 0 индекса это: " + ch1);
        System.out.println("Символ 21 индекса это: " + ch2);
        System.out.println("Символ 7 индекса это: " + ch3);
    }
}
