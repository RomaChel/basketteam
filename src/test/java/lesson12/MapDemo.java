package lesson12;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {

    public static void main(String[] args) {
        HashMap<String, String> myMap = new HashMap<>();
        myMap.put("Vasil", "07.09.1987");
        myMap.put("Kate", "12.10.1971");
        myMap.put("Leonid", "11.01.1991");
        for (Map.Entry<String, String> entry : myMap.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
        System.out.println("Value: " + myMap.get("Kate"));

       /* myMap.remove("Vasil");
        System.out.println("\nAfter removing a key: ");
        for (Map.Entry<String, String> entry : myMap.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }*/

        System.out.println("Do we have 'Vasil'? " + myMap.containsKey("Vasil"));
        System.out.println("Do we have 'Value of Vasil'? " + myMap.containsValue("07.09.1987"));
        System.out.println("Do we have 'Victoria'? " + myMap.containsKey("Victoria"));
        System.out.println("The size of map is: " + myMap.size());
    }
}
