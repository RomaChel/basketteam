package lesson12;

import java.util.*;

public class ArrayListAddDemo {

    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<>();

        System.out.println("Начальный размер arrayList: " + arrayList.size());

        arrayList.add("C");
        arrayList.add("A");
        arrayList.add("E");
        arrayList.add("B");
        arrayList.add("D");
        arrayList.add("F");
        arrayList.add("F");
        arrayList.add(2,"E2");
        arrayList.set(0, "C2");

        System.out.println("Размер arrayList после добавления: " + arrayList.size());
        System.out.println("Содержимое arrayList: " + arrayList);

        System.out.println(arrayList.get(5));
        System.out.println(arrayList.indexOf("D"));
        System.out.println("---------------------------------------------------");

        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }
    }
}
