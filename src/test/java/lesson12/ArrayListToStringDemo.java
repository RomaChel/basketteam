package lesson12;

import java.util.Arrays;
import java.util.List;

public class ArrayListToStringDemo {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("C", "A", "E", "B", "D", "F");
        Object[] objectArray = list.toArray();
        for (int i = 0; i < objectArray.length; i++) {
            System.out.println(objectArray[i]);
        }
        System.out.println(Arrays.toString(objectArray));
    }
}
class ArrayListToStringDemo2 {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("C", "A", "E", "B", "D", "F");
        //1 варик
        String[] stringArray1 = new String[list.size()];
        list.toArray(stringArray1);
        System.out.println(Arrays.toString(stringArray1));
        //2 варик
        String[] stringArray2 = list.toArray(new String[0]);
        System.out.println(Arrays.toString(stringArray2));
    }
}