package lesson12;

import java.util.LinkedHashSet;
import java.util.Set;

public class LinkedHashSetDemo {

    public static void main(String[] args) {
        Set<String> myLinkedSet = new LinkedHashSet<>();

        myLinkedSet.add("Украина");
        myLinkedSet.add("Турция");
        myLinkedSet.add("США");
        myLinkedSet.add("Польша");
        myLinkedSet.add("Франция");
        myLinkedSet.add("Украина");
        myLinkedSet.add("Германия");

        System.out.println(myLinkedSet);
    }
}
