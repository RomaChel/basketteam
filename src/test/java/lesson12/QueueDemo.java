package lesson12;

import java.util.LinkedList;
import java.util.Queue;

public class QueueDemo {

    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();

        queue.offer("Харьков");
        queue.offer("Киев");
        queue.offer("Житомир");
        queue.offer("Львов");

        System.out.println(queue.peek());

        String town;
        while ((town = queue.poll()) != null) {
            System.out.println("Удалился объект: " + town);
            System.out.println("Остались объект(ы): " + queue);
        }
    }
}
