package lesson12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListRemoveDemo {

    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<>();

        System.out.println("Начальный размер arrayList: " + arrayList.size());

        arrayList.add("C");
        arrayList.add("A");
        arrayList.add("E");
        arrayList.add("B");
        arrayList.add("D");
        arrayList.add("F");
        arrayList.add("F");
        arrayList.add(2, "E2");
        arrayList.set(0, "C2");

        System.out.println("Размер arrayList после добавления: " + arrayList.size());
        System.out.println("Содержимое arrayList: " + arrayList);

        arrayList.remove("E");
        arrayList.remove(1);

        System.out.println("Размер arrayList после удаления: " + arrayList.size());
        System.out.println("Содержимое arrayList: " + arrayList);
    }
}

class ArrayListRemoveAllDemo {

    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<>();
        List<String> removeElements = Arrays.asList("B", "F", "D", "0");

        System.out.println("Начальный размер arrayList: " + arrayList.size());

        arrayList.add("C");
        arrayList.add("A");
        arrayList.add("E");
        arrayList.add("B");
        arrayList.add("D");
        arrayList.add("F");
        arrayList.add("F");
        arrayList.add(2, "E2");
        arrayList.set(0, "C2");


        System.out.println("Размер arrayList после добавления: " + arrayList.size());
        System.out.println("Содержимое arrayList: " + arrayList);
        System.out.println("---------------------------------------------------------");

        arrayList.removeAll(removeElements);
        System.out.println("Содержимое arrayList после removeAll: "
                + arrayList);

    }
}

class ArrayListRetainAllDemo {
    public static void main(String[] args) {
        List<String> arrayList1 = new ArrayList<>();
        List<String> arrayList2 = Arrays.asList("F", "FF", "E", "C");

        arrayList1.add("A");
        arrayList1.add("A");
        arrayList1.add("B");
        arrayList1.add("C");
        arrayList1.add("D");
        arrayList1.add("E");
        arrayList1.add("F");
        arrayList1.add("F");

        arrayList1.retainAll(arrayList2);
        System.out.println(arrayList1);
    }
}