package lesson12;

import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetDemo {

    public static void main(String[] args) {
        SortedSet<String> mySortSet = new TreeSet<>();

        mySortSet.add("Харьков");
        mySortSet.add("Киев");
        mySortSet.add("Львов");
        mySortSet.add("Кременчуг");
        mySortSet.add("Харьков");

        System.out.println(mySortSet);

        SortedSet<String> subSet = mySortSet.subSet("Киев", "Харьков");
        System.out.println(subSet);
    }
}
