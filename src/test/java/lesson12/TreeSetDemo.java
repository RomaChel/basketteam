package lesson12;

import java.util.SortedSet;
import java.util.TreeSet;

public class TreeSetDemo {

    public static void main(String[] args) {

        SortedSet<String> set = new TreeSet<>();

        set.add("Украина");
        set.add("Турция");
        set.add("США");
        set.add("Польша");
        set.add("Франция");
        set.add("Украина");
        set.add("Германия");
        set.add("Андора");
        System.out.println(set);

        SortedSet<Integer> set1 = new TreeSet<>();

        set1.add(2);
        set1.add(10);
        set1.add(1);
        set1.add(5);
        set1.add(8);
        set1.add(3);
        set1.add(4);
        System.out.println(set1);
    }
}
