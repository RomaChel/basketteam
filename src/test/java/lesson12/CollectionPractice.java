package lesson12;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionPractice {

    public static void main(String[] args) {
        int[] array = new int[15];

        Collection collection = new ArrayList();
        collection.add("1");
        collection.add("2");
        collection.add("3");
        collection.add("4");
        collection.add("4");
        collection.add("3");

        collection.remove("3");
        collection.remove("4");
        //System.out.println(collection.size());

/*        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }*/
        for (Object o : collection) {
            System.out.println(o);
        }
    }
}
