package lesson12;

import java.util.HashSet;
import java.util.Set;

public class HashSetDemo {

    public static void main(String[] args) {

        Set<String> mySet = new HashSet<>();

        mySet.add("Хрьков");
        mySet.add("Одесса");
        mySet.add("Виница");
        mySet.add("Киев");
        mySet.add("Хрьков");
        mySet.add("Николаев");

        System.out.println(mySet);

        for (Object o : mySet) {
            System.out.println(o);
        }
    }
}
