package calculator;

import java.util.Scanner;

public class Calculator {

    public static double getNumber() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число:");
        if (scan.hasNextDouble()) {
            return scan.nextDouble();
        } else {
            System.out.println("Ошибка при вводе. Повторите ввод");
            return getNumber();
        }
    }

    public static char getOperation() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Выберите номер операции:\n1 - прибавить\n2 - отнять\n3 - умножить\n4 - разделить");
        int operationNumber = 0;
        if (scan.hasNextInt()) {
            operationNumber = scan.nextInt();
        } else {
            System.out.println("Вы ввели не существующую операцию! Повторите ввод!");
            return getOperation();
        }
        switch (operationNumber) {
            case 1:
                return '+';
            case 2:
                return '-';
            case 3:
                return '*';
            case 4:
                return '/';
            default:
                System.out.println("Неправельный опператор! Повторите ввод!");
                return getOperation();
        }
    }

    private static double add(double num1, double num2) {
        return num1 + num2;
    }

    private static double division(double num1, double num2) {
        double res = 0;
        if (num2 != 0.0) {
            res =  num1 / num2;
        } else {
            System.out.println("На 0 делить нельзя! Повторите ввод");
            //System.exit(-1);
            // return Double.NaN;
            return calc(getNumber(), getNumber(), getOperation());
        }
        return res;
    }


    public static double calc(double num1, double num2, char operation) {
        switch (operation) {
            case '+':
                return add(num1, num2);
            case '/':
                return division(num1, num2);
            default:
                System.out.println("Ошибка операции. Повторите ввод");
                return calc(num1, num2, operation);
        }
    }
}
