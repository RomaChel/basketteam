package lesson4;

public class DoWhileLoop {

    public static void main(String[] args) {

        int i = 8;
        do {
            System.out.println(i + " ");
            i++;
            System.out.println(i + " ");
        } while (i >= 9);
    }
}