package lesson4;

import java.util.Scanner;

public class ConditionalOperator_switch {

    public static void main(String[] args) {
        numberCompare();
    }

    public static void numberCompare() {
        System.out.println("Введите чсло 1 или 2 или 3");
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        switch (number) {
            case 1:
                System.out.println("Вы ввели число 1");
                break;
            case 2:
                System.out.println("Вы ввели число 2");
                break;
            case 3:
                System.out.println("Вы ввели число 3");
                break;
            default:
                System.out.println("Вы ввели не правильное число!!!");
        }
    }
}
