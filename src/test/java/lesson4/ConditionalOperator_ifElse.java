package lesson4;

import java.util.Scanner;

public class ConditionalOperator_ifElse {

    public static void main(String[] args) {
        numberCompare();
    }

    public static void numberCompare() {
        System.out.println("Введите чсло 1 или 2 или 3");
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        if (number == 1) {
            System.out.println("Вы ввели число 1");
        } else if (number == 2) {
            System.out.println("Вы ввели число 2");
        } else if (number == 3) {
            System.out.println("Вы ввели число 3");
        } else {
            System.out.println("Вы ввели числа не равные 1, 2 или 3");
        }
    }
}
