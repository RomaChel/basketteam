package lesson4;

public class WhileLoop {

    public static void main(String[] args) {
        int a = 1;
        while (a<=7) {
            System.out.print(a + " ");
            a++;
        }

        int b = 7;
        while (b >= 1) {
            System.out.print(b + " ");
            b--;
        }

        int v = 1;
        while (v < 0) {
            System.out.println(v + " ");
            v++;
        }

        int g = 1;
        while(true) {
            System.out.println(g);
            g++;
        }
    }
}
