package lesson4;

public class OperatorBreak {

    public static void main(String[] args) {
        int i;
        int a;

        i = 1;
        a = 3;

        while (i <= 5) {
            System.out.println(i + ": цикл выполнится!");
            if (i == a)
                break;
            i++;
        }

        System.out.println("-------------------------------------------------------------------");


        for (int g = 1; g <= 3; g++) {
            System.out.println("Это первый цикл! Я выполнюсь " + g + "-ый раз");
            for (int m = 1; m <= 10; m++) {
                System.out.println("         Это второй циклЙ Я выполняюсь " + m + "-ый раз");
                if (m == 5)
                    break;
            }
        }
    }
}
